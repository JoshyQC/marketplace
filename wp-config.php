 <?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'creedu' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'o;djhMfCWQh;<>p,$Ek@<BeuJwA rB:*0<tt0#<s&W5,](0 %`YLMTc[Tox|X~ZY' );
define( 'SECURE_AUTH_KEY',  'sS?DSa,6wk:44AX-w+.sua-%{u;?7C@Z{Ng$S[:(M~I(gYrk^nk~FWDK.A%*SOVW' );
define( 'LOGGED_IN_KEY',    '`c%GJ,0Yqs(0?I#+}kKWj9avAU(mK$Ev^jTJAGrO|w<d`-Kmuv^:3J1sW~dgOl,7' );
define( 'NONCE_KEY',        '|]YW??`n>g$=JCSD~qo+dq!N0K-T|+qRlN]_t:hl?T!0kun>1H~;^>8_,Ns2Sd l' );
define( 'AUTH_SALT',        '^.b(<)COj6W,PrUHRCMYWY$Dj;#)LK2sr4sBN<XO[n8=!pW/`GKd#B*Yzvn/Kh:}' );
define( 'SECURE_AUTH_SALT', 'sM+kh,Q9t)Ps`JaP7hT8#3Tj}tc|=#V#4=a%m2hb.4>@1o`SmFPrma#jH-m,}U52' );
define( 'LOGGED_IN_SALT',   '{;cIW*U2w0|J_h4oVk58v16ub!eC4/Iuq[l/3Btid8iE3521X{wvUsC~~4n|7Ow6' );
define( 'NONCE_SALT',       'q0c Stg8{~K!0+g!Ok4}{Y[M]^o+v:hywwe-lyOoYKWH|A{iALQw`fMj!&>p4~1:' );
define('JWT_AUTH_SECRET_KEY', 'Genesis2020');
define('JWT_AUTH_CORS_ENABLE', true);

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'cree_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/**Administrative Memory */
define('WP_MAX_MEMORY_LIMIT', '512M');

/**Public Memory */
define('WP_MEMORY_LIMIT', '512M');

define('WP_HOME','http://localhost:8080/credito-educativo/');

define('WP_SITEURL','http://localhost:8080/credito-educativo/');


/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
