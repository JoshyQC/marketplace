﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="burocree.index" %>

<%--<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        </div>
    </form>
</body>
</html>--%>


<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">

    <!-- Title Page-->
    <title>Buro</title>
    <!-- JS -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="js/jquery.min.js"></script>
    <!-- Icons font CSS-->
    <link href="css/Style.css" rel="stylesheet" />
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/main.css" rel="stylesheet" media="all">
</head>

<body>
    <div class="page-wrapper bg-gra-02 p-t-130 p-b-100 font-poppins">
        <div class="wrapper wrapper--w680">
            <div class="card card-4">
                <div class="card-body">
                    <h2 class="title-buro">SOLICITA TU CRÉDITO EDUCATIVO AQUÍ</h2>
                    <br />
                    <form id="formularioBuro" runat="server" novalidate>
                        <div class="input-group">
                            <label class="label">PRIMER NOMBRE</label>
                            <div class="rs-select2 js-select-simple select--no-search">
                                <asp:TextBox class="input--style-4" ID="TxtPrimerNombre" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="ReqPrimerNombre" runat="server" ControlToValidate="TxtPrimerNombre" ErrorMessage="Este campo es requerido, por favor llenarlo" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="input-group">
                            <label class="label">SEGUNDO NOMBRE</label>
                            <div class="rs-select2 js-select-simple select--no-search">
                                <asp:TextBox ID="TxtSegundoNombre" class="input--style-4" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="input-group">
                            <label class="label">PRIMER APELLIDO</label>
                            <div class="rs-select2 js-select-simple select--no-search">
                                <asp:TextBox ID="TxtPrimerApellido" class="input--style-4" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="ReqPrimerApellido" runat="server" ControlToValidate="TxtPrimerApellido" ErrorMessage="Este campo es requerido, por favor llenarlo" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="input-group">
                            <label class="label">SEGUNDO APELLIDO</label>
                            <div class="rs-select2 js-select-simple select--no-search">
                                <asp:TextBox ID="TxtSegundoApellido" class="input--style-4" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="input-group">
                            <label class="label">TELÉFONO CELULAR</label>
                            <div class="rs-select2 js-select-simple select--no-search">
                                <asp:TextBox ID="TxtTelefono" class="input--style-4" TextMode="Number" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="input-group">
                            <label class="label">DPI</label>
                            <div>
                                <asp:TextBox ID="TxtDPI" class="input--style-4" TextMode="Number" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="ReqDPIPasaporte" runat="server" ControlToValidate="TxtDPI" ErrorMessage="Este campo es requerido, por favor llenarlo" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="input-group">
                            <label class="label">FECHA DE NACIMIENTO</label>
                            <div class="input-group-icon">
                                <asp:TextBox ID="TxtFechaNac" class="input--style-4 js-datepicker" runat="server"></asp:TextBox>
                                <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>
                            </div>
                        </div>
                        <div class="input-group">
                            <div class="rs-select2 js-select-simple select--no-search">
                                <label><asp:CheckBox ID="CbxTerminosCondiciones" runat="server"/> He leído y aceptado la Política de Privacidad.</label>
                            </div>
                        </div>
                        <div class="p-t-15">
                            <asp:Button ID="BtnConsultar" runat="server"   OnClientClick="return false" class="btn btn--radius-2 btn--blue" Text="Enviar" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



    <!-- Jquery JS-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="vendor/select2/select2.min.js"></script>
    <script src="vendor/datepicker/moment.min.js"></script>
    <script src="vendor/datepicker/daterangepicker.js"></script>

    <!-- Main JS-->
    <script src="js/global.js"></script>

</body>
<!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->
