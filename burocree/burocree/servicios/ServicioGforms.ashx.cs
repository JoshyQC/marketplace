﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace burocree.servicios
{
    /// <summary>
    /// Descripción breve de ServicioGforms
    /// </summary>
    public class ServicioGforms : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            
            string json = LoadForm();
            context.Response.ContentType = "text/json";
            context.Response.Write(json);
        }

        private string LoadForm()
        {
            string iSql = "SELECT FOR_ESQUEMA AS JSON FROM for_formularios WHERE ID=90";
            string Conexion = "Server=10.1.2.82;Database=gforms;Uid=usrgforms;Pwd=Innova19;";

            using (MySqlConnection Cnn = new MySqlConnection(Conexion))
            {
                MySqlCommand Cmd = new MySqlCommand(iSql, Cnn);
                Cmd.CommandTimeout = 0;
                Cnn.Open();
                MySqlDataReader Reader = Cmd.ExecuteReader();
                string Json = "";

                while (Reader.Read())
                {
                    try
                    {
                        Json = Reader.GetString(0);
                    }
                    catch (Exception ex)
                    {
                        ex.Message.ToString();
                    }
                }
                Cnn.Close();
                Cmd.Dispose();

                return Json;
            }
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}