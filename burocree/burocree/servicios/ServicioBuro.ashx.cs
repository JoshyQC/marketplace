﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace burocree.servicios
{
    /// <summary>
    /// Descripción breve de ServicioBuro
    /// </summary>
    public class ServicioBuro : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            string dpi = context.Request.QueryString["dpi"];
            string pnombre = context.Request.QueryString["pnombre"];
            string snombre = context.Request.QueryString["snombre"];
            string papellido = context.Request.QueryString["papellido"];
            string sapellido = context.Request.QueryString["sapellido"];
            string fecnac = context.Request.QueryString["fecnac"];

            string json = "";
            json = Buro(dpi, pnombre, snombre, papellido, sapellido, fecnac);

            context.Response.ContentType = "text/json";
            context.Response.Write(json);

        }

        public string Buro(string dpi, string pnombre, string snombre, string papellido, string sapellido, string fecnac)
        {
            if (snombre == string.Empty)
            {
                snombre = "0";
            }
            if (sapellido == string.Empty)
            {
                sapellido = "0";
            }

            var client = new RestClient("http://192.168.9.12/api/gestiones/" + dpi + "/" + pnombre + "/" + snombre + "/" + papellido + "/" + sapellido + "/" + fecnac + "/Primario");
            var request = new RestRequest(Method.GET);
            request.AddHeader("postman-token", "79924295-8774-1faf-7af1-8e6fb3c5d866");
            request.AddHeader("cache-control", "no-cache");
            IRestResponse response = client.Execute(request);

            return response.Content.ToString();
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

    }
}