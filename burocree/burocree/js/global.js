(function ($) {
    'use strict';
    /*==================================================================
        [ Daterangepicker ]*/
    try {
        $('.js-datepicker').daterangepicker({
            "singleDatePicker": true,
            "showDropdowns": true,
            "autoUpdateInput": false,
            locale: {
                format: 'YYYY/MM/DD'
            },
        });
    
        var myCalendar = $('.js-datepicker');
        var isClick = 0;
    
        $(window).on('click',function(){
            isClick = 0;
        });
    
        $(myCalendar).on('apply.daterangepicker',function(ev, picker){
            isClick = 0;
            $(this).val(picker.startDate.format('YYYY/MM/DD'));
    
        });
    
        $('.js-btn-calendar').on('click',function(e){
            e.stopPropagation();
    
            if(isClick === 1) isClick = 0;
            else if(isClick === 0) isClick = 1;
    
            if (isClick === 1) {
                myCalendar.focus();
            }
        });
    
        $(myCalendar).on('click',function(e){
            e.stopPropagation();
            isClick = 1;
        });
    
        $('.daterangepicker').on('click',function(e){
            e.stopPropagation();
        });
    
    
    } catch(er) {console.log(er);}
    /*[ Select 2 Config ]
        ===========================================================*/
    
    try {
        var selectSimple = $('.js-select-simple');
    
        selectSimple.each(function () {
            var that = $(this);
            var selectBox = that.find('select');
            var selectDropdown = that.find('.select-dropdown');
            selectBox.select2({
                dropdownParent: selectDropdown
            });
        });
    
    } catch (err) {
        console.log(err);
    }

    //FIELDS VALIDATION

    //sweet alert function
    function SweetAlert(mensaje, tipo) {
        const Toast = Swal.mixin({
            toast: true,
            position: 'center-end',
            showConfirmButton: false,
            timer: 5000
        });

        Toast.fire({
            type: tipo,
            title: mensaje
        })

    }

    //Birth validation 
    $("#TxtFechaNac").bind('focusout', function (e) {

        var today = new Date();
        var mm = today.getMonth() + 1;
        var yy = today.getFullYear();
        var fechaFormulario = $("#TxtFechaNac").val().split('/');
        var monthUser = fechaFormulario[1] - 1;
        var yearUser = fechaFormulario[0];
        var age = yy - yearUser;

        if ( age < 18 ) {
            SweetAlert("Fecha de Nacimiento Invalida, Verifique", "error");
            $("#TxtFechaNac").val("");
        }
    });

    $("#TxtDPI").bind('focusout', function (e) {

        var cui = $('#TxtDPI').val();
        var cuiRegExp = /^[0-9]{4}\s?[0-9]{5}\s?[0-9]{4}$/;

        if (!cuiRegExp.test(cui)) {
            SweetAlert('DPI incorrecto, por favor verifique','success');
            $("#TxtDPI").val("");
        }
    });

    $("#BtnConsultar").click(function () {

        if ($("#CbxTerminosCondiciones").is(':not(:checked)')) {

            SweetAlert("Debe aceptar las politicas de privacidad");
        } 
        else {

            // Realizando consulta de buro 
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1;
            var min = today.getMinutes();
            var sec = today.getSeconds();
            var milisec = today.getMilliseconds();

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }

            today = dd + mm + yyyy + min + sec + milisec;

            var id_order = today;
            var dpi = $("#TxtDPI").val();
            var p_nombre = $("#TxtPrimerNombre").val();
            var s_nombre = $("#TxtSegundoNombre").val();
            var p_apellido = $("#TxtPrimerApellido").val();
            var s_apellido = $("#TxtSegundoApellido").val();
            var fecha_nac = $("#TxtFechaNac").val().replace(/\//g, "");
            var resultado = "";
            var id_prueba = Math.floor(Math.random() * 90000) + 10000;

            if (id_order != "" && dpi != "" && p_nombre != "" && p_apellido != "" && fecha_nac != "") {

                if (s_nombre == "") {
                    s_nombre = '0';
                }
                if (s_apellido == "") {
                    s_apellido = '0';
                }

                $.ajax({
                    type: "GET",
                    url: "servicios/ServicioBuro.ashx",
                    data: "dpi=" + dpi + "&pnombre=" + p_nombre + "&snombre=" + s_nombre + "&papellido=" + p_apellido + "&sapellido=" + s_apellido + "&fecnac=" + fecha_nac,
                    success: function (json) {
                        if (json[0].valor != null) {
                            resultado = json[0].valor;

                            if (resultado == 'APROBADO'){
                                swal.fire({
                                    title: '',
                                    text: 'Se le presentara un formulario, el cual debe llenar sus datos, desea llenarlo?',
                                    icon: 'success',
                                    showCancelButton: true,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    cancelButtonText: 'NO',
                                    confirmButtonText: 'SI',
                                }).then(function (result) {
                                    if (result.value) {
                                        window.open('/FormularioGforms.aspx', "newWin", "width=" + screen.availWidth + ",height=" + screen.availHeight);
                                    }
                                });
                            } else {
                                swal.fire({
                                    title: resultado,
                                    text: 'Gracias por preferirnos, nos estaremos comunicando contigo en breve',
                                    icon: 'error',
                                    showCancelButton: false,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'Ok',
                                });
                            }
                        }
                        else {
                            SweetAlert("Intentlo de nuevo por favor");
                        }
                    },
                    error: function () {
                        console.log("error123");
                    }
                });

            }
            else {
                SweetAlert("Verifique los campos Ingresados e intÚntelo de nuevo.");
            }

        }
    });

})(jQuery);