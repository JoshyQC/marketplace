﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace burocree
{
    public partial class index : System.Web.UI.Page
    {
        public void MsgBox(String titulo, String mensaje, String tipo_mensaje, Page pg, Object obj, Int32 tipo, String codigo)
        {
            string s = "<script> ";   // Se Utiliza el componente Sweet Alert, se realiza funcion para diferentes tipos de mensaje

            if (tipo == 1)
            {

                s += " Swal.fire(";
                s += "'" + titulo + "',";
                s += "'" + mensaje + "',";
                s += "'" + tipo_mensaje + "')";

            }
            else if (tipo == 2)
            {


                s += " swal.fire({";
                s += " title: '" + titulo + "',";
                s += " text: '" + mensaje + "',";
                s += " type: '" + tipo_mensaje + "',";
                s += " showCancelButton: false,";
                s += " confirmButtonColor: '#3085d6',";
                s += " cancelButtonColor: '#d33',";
                s += " confirmButtonText: 'Ok'";
                s += " }).then(function () {";
                s += " window.open('/emprendimiento/reporte/carnetContainer.aspx?cod-emprende=" + codigo + "'); ";
                s += "";
                s += "   }) ";


            }

            s += "</script>";

            Type cstype = obj.GetType();
            ClientScriptManager cs = pg.ClientScript;
            cs.RegisterClientScriptBlock(cstype, s.ToString(), s.ToString());
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //MsgBox("Formulario Grabado Exitosamente", "Se ha grabado la informacion correctamente, porfavor imprimir carnert.", "success", this.Page, this, 1, "test");
        }

    }
}