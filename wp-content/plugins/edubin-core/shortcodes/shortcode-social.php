<?php
/**
 *
 * Social Shortcode
 *
 */


class Edubin_Shortcode_Social {


    /**
     *
     * Shortcode Name
     * @var string
     */

    private $name = 'edubin-social';


    /**
     * Instance of class
     */
    private static $instance;

    /**
     * Initialization
     */
    public static function init() {
        if ( null === self::$instance ) {
            self::$instance = new self;
        }

        return self::$instance;
    }


    private function __construct() {

        add_shortcode( $this->name, array( $this, 'create_social_box_shortcode' ) );
    }


    /**
     * Shortcode Function
     *
     * @param $atts
     * @return string

      Example: [edubin-social facebook-link="https://fb.com" twitter-link="http://twittter.com/" youtube-link="https://www.youtube.com/" instagram-link="https://www.instagram.com/"]
      
     */


    public function create_social_box_shortcode( $atts ) {

        ob_start();

        $default = array(
            'facebook-link' => '',
            'twitter-link' => '',
            'youtube-link' => '',
            'instagram-link' => ''
        );

        $social = shortcode_atts( $default, $atts );


        ?>
            <div class="edubin-social">
            <?php if ($social['facebook-link']) : ?>
                <a class="edubin-social-icon" href="<?php echo esc_url($social['facebook-link']); ?>" title="Facebook" target="_blank">
                    <i class="glyph-icon flaticon-facebook-logo"></i>
                </a>
            <?php endif; ?>
            <?php if ($social['twitter-link']) : ?>
                <a class="edubin-social-icon" href="<?php echo esc_url($social['twitter-link']); ?>" title="Twitter" target="_blank">
                    <i class="glyph-icon flaticon-twitter-logo-silhouette"></i>
                </a>
            <?php endif; ?>
            <?php if ($social['youtube-link']) : ?>
                <a class="edubin-social-icon" href="<?php echo esc_url($social['youtube-link']); ?>" title="Youtube" target="_blank">
                    <i class="glyph-icon flaticon-youtube-logo"></i>
                </a>
            <?php endif; ?>
            <?php if ($social['instagram-link']) : ?>
                <a class="edubin-social-icon" href="<?php echo esc_url($social['instagram-link']); ?>" title="Instagram" target="_blank">
                    <i class="glyph-icon flaticon-instagram-logo"></i>
                </a>
            <?php endif; ?>
            </div>


    <?php
        $output = ob_get_clean();

        return $output;

    }


}