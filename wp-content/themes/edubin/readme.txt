=== Edubin===
Theme Name: Edubin 
Theme URI: https://wpsprite.com/wordpress/edubin/landing/
Description: Edubin Education LMS WordPress Theme.
Author: Pixelcurve
Author URI: https://wpsprite.com/
Version: 3.0.5
Tags: two-columns, three-columns, left-sidebar, sticky-post, theme-options, translation-ready.
Text Domain: edubin
License: GNU General Public License v2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html


== Description ==
Edubin Education LMS WordPress Theme.

For more information about edubin please go to https://wpsprite.com/

== Installation ==

Please see installation guide in Documentation. here https://wpsprite.com/support/docs/edubin/