<?php
/**
 * Displays header site branding
 *
 * @package Edubin
 * Version: 1.0.0
 */
$defaults = edubin_generate_defaults();
$sticky_logo = get_theme_mod( 'sticky_logo', $defaults['sticky_logo'] );
$header_variations = get_theme_mod( 'header_variations', $defaults['header_variations'] );

?>

<div class="site-branding d-inline-block">
    <?php if( $sticky_logo and $header_variations == 'header_v3'  ) : ?>
         <div class="custom-logo sticky-logo">
           <a href="<?php echo esc_url( home_url( '/' ) ); ?>"> <img src="<?php echo esc_url($sticky_logo); ?>" alt="<?php bloginfo( 'name' ); ?>"></a>
        </div>
         <div class="custom-logo default-logo">
           <?php the_custom_logo(); ?>
        </div>
	<?php elseif(the_custom_logo()):?>
        <div class="custom-logo">
            <?php the_custom_logo(); ?>
        </div>
	<?php  elseif ( is_front_page() ) : ?>

        <?php if (!has_custom_logo()): ?>
            <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>

            <?php $description = get_bloginfo( 'description', 'display' );
                if ( $description || is_customize_preview()) :
            ?>
            <p class="site-description"><?php echo wp_kses_data($description); ?></p>
            <?php endif;  ?>
        <?php endif; ?>
    <?php else : ?>
        <?php if (!has_custom_logo()): ?>
            <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
        <?php 

        $description = get_bloginfo( 'description', 'display' );
        if ( $description || is_customize_preview()) :
        ?>
            <p class="site-description"><?php echo wp_kses_data($description); ?></p>
        <?php endif;  endif;  ?>

    <?php endif;  ?>

</div><!-- .site-branding -->
