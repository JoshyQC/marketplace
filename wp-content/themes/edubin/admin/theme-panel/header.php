<?php   // Header top section
    $wp_customize->add_section( 'header_top_section',
        array(
            'title' => esc_html__( 'Header Top', 'edubin' ),
            'panel' => 'header_naviation_panel'
        )
    );

    // Header section
    $wp_customize->add_section( 'main_header_section',
        array(
            'title' => esc_html__( 'Main Header', 'edubin' ),
            'panel' => 'header_naviation_panel'
        )
    );
    
    /**
     * Add our Social Icons Section
     */
    $wp_customize->add_section( 'social_icons_section',
        array(
            'title' => esc_html__( 'Social Icons', 'edubin' ),
            'description' => esc_html__( 'Drag and drop the URLs to rearrange their order.', 'edubin' ),
            'panel' => 'header_naviation_panel'
        )
    );

    /**
     * Add our Contact Section
     */
    $wp_customize->add_section( 'contact_section',
        array(
            'title' => esc_html__( 'Contact', 'edubin' ),
            'description' => esc_html__( 'Add your phone number to the site header bar.', 'edubin' ),
            'panel' => 'header_naviation_panel'
        )
    );

    /**
     * Add our Search Section
     */
    $wp_customize->add_section( 'search_section',
        array(
            'title' => esc_html__( 'Search', 'edubin' ),
            'description' => esc_html__( 'Add a search icon to your primary navigation menu.', 'edubin' ),
            'panel' => 'header_naviation_panel'
        )
    );

    // Header top show/hide
    $wp_customize->add_setting( 'header_top_show',
        array(
            'default' => $this->defaults['header_top_show'],
            'transport' => 'refresh',
            'sanitize_callback' => 'edubin_switch_sanitization'
        )
    );
    $wp_customize->add_control( new Edubin_Toggle_Switch_Custom_control( $wp_customize, 'header_top_show',
        array(
            'label' => esc_html__( 'Enable', 'edubin' ),
            'section' => 'header_top_section'
        )
    ) );
    
    // Top email
    $wp_customize->add_setting( 'top_email',
        array(
            'default' => $this->defaults['top_email'],
            'transport' => 'refresh',
            'sanitize_callback' => 'wp_filter_nohtml_kses'
        )
    );
    $wp_customize->add_control( 'top_email',
        array(
            'label' => esc_html__( 'Email', 'edubin' ),
            'type' => 'text',
            'section' => 'header_top_section'
        )
    );
    // Top phone
    $wp_customize->add_setting( 'top_phone',
        array(
            'default' => $this->defaults['top_phone'],
            'transport' => 'refresh',
            'sanitize_callback' => 'wp_filter_nohtml_kses'
        )
    );

    $wp_customize->add_control( 'top_phone',
        array(
            'label' => esc_html__( 'Phone', 'edubin' ),
            'type' => 'text',
            'section' => 'header_top_section'
        )
    );
    // Top massage
    $wp_customize->add_setting( 'top_massage',
        array(
            'default' => $this->defaults['top_massage'],
            'transport' => 'refresh',
            'sanitize_callback' => 'wp_filter_nohtml_kses'
        )
    );

    $wp_customize->add_control( 'top_massage',
        array(
            'label' => esc_html__( 'Top Massage', 'edubin' ),
            'type' => 'text',
            'section' => 'header_top_section'
        )
    );
    // Login/Register show/hide
    $wp_customize->add_setting( 'login_reg_show',
        array(
            'default' => $this->defaults['login_reg_show'],
            'transport' => 'refresh',
            'sanitize_callback' => 'edubin_switch_sanitization'
        )
    );
    $wp_customize->add_control( new Edubin_Toggle_Switch_Custom_control( $wp_customize, 'login_reg_show',
        array(
            'label' => esc_html__( 'Login/Register', 'edubin' ),
            'section' => 'header_top_section'
        )
    ) );
    // Custom login link
    $wp_customize->add_setting( 'custom_login_link',
        array(
            'default' => $this->defaults['custom_login_link'],
            'transport' => 'refresh',
            'sanitize_callback' => 'esc_url_raw'
        )
    );
    $wp_customize->add_control( 'custom_login_link',
        array(
            'label' => esc_html__( 'Custom Login Link', 'edubin' ),
            'section' => 'header_top_section',
            'type' => 'url'
        )
    );
    // Custom register link
    $wp_customize->add_setting( 'custom_register_link',
        array(
            'default' => $this->defaults['custom_register_link'],
            'transport' => 'refresh',
            'sanitize_callback' => 'esc_url_raw'
        )
    );
    $wp_customize->add_control( 'custom_register_link',
        array(
            'label' => esc_html__( 'Custom Register Link', 'edubin' ),
            'section' => 'header_top_section',
            'type' => 'url'
        )
    );
    // Custom logout link
    $wp_customize->add_setting( 'custom_logout_link',
        array(
            'default' => $this->defaults['custom_logout_link'],
            'transport' => 'refresh',
            'sanitize_callback' => 'esc_url_raw'
        )
    );
    $wp_customize->add_control( 'custom_logout_link',
        array(
            'label' => esc_html__( 'Custom Logout Page Link', 'edubin' ),
            'section' => 'header_top_section',
            'type' => 'url'
        )
    );

 // Text color
    $wp_customize->add_setting( 'header_top_text_color',
        array(
            'default' => $this->defaults['header_top_text_color'],
            'transport'         => 'refresh',
            'sanitize_callback' => 'edubin_hex_rgba_sanitization',
        )
    );
    $wp_customize->add_control( new Edubin_Customize_Alpha_Color_Control( $wp_customize, 'header_top_text_color',
        array(
            'label' => esc_html__( 'Text Color', 'edubin' ),
            'section' => 'header_top_section',
            'show_opacity' => true,
            'palette' => array(
                '#000',
                '#fff',
                '#df312c',
                '#df9a23',
                '#eef000',
                '#7ed934',
                '#1571c1',
                '#8309e7'
            )
        )
    ) );

    // Link color
    $wp_customize->add_setting( 'header_top_link_color',
        array(
            'default' => $this->defaults['header_top_link_color'],
            'transport'         => 'refresh',
            'sanitize_callback' => 'edubin_hex_rgba_sanitization',
        )
    );
    $wp_customize->add_control( new Edubin_Customize_Alpha_Color_Control( $wp_customize, 'header_top_link_color',
        array(
            'label' => esc_html__( 'Link Color', 'edubin' ),
            'section' => 'header_top_section',
            'show_opacity' => true,
            'palette' => array(
                '#000',
                '#fff',
                '#df312c',
                '#df9a23',
                '#eef000',
                '#7ed934',
                '#1571c1',
                '#8309e7'
            )
        )
    ) );
    // Background color
    $wp_customize->add_setting( 'header_top_bg_color',
        array(
            'default' => $this->defaults['header_top_bg_color'],
            'transport'         => 'refresh',
            'sanitize_callback' => 'edubin_hex_rgba_sanitization',
        )
    );
    $wp_customize->add_control( new Edubin_Customize_Alpha_Color_Control( $wp_customize, 'header_top_bg_color',
        array(
            'label' => esc_html__( 'Background Color', 'edubin' ),
            'section' => 'header_top_section',
            'show_opacity' => true,
            'palette' => array(
                '#000',
                '#fff',
                '#df312c',
                '#df9a23',
                '#eef000',
                '#7ed934',
                '#1571c1',
                '#8309e7'
            )
        )
    ) );
    // Header variations
    $wp_customize->add_setting( 'header_variations',
        array(
            'default' => $this->defaults['header_variations'],
            'transport' => 'refresh',
            'sanitize_callback' => 'edubin_radio_sanitization'
        )
    );
    $wp_customize->add_control( 'header_variations',
        array(
            'label' => esc_html__( 'Header Variations', 'edubin' ),
            'section' => 'main_header_section',
            'type' => 'select',
            'choices' => array(
                'header_v1' => esc_html__( 'Style 1', 'edubin' ),
                'header_v2' => esc_html__( 'Style 2', 'edubin' ),
            )
        )
    );

    // Sticky header enable
    $wp_customize->add_setting( 'sticky_header_enable',
        array(
            'default' => $this->defaults['sticky_header_enable'],
            'transport' => 'refresh',
            'sanitize_callback' => 'edubin_switch_sanitization'
        )
    );
    $wp_customize->add_control( new Edubin_Toggle_Switch_Custom_control( $wp_customize, 'sticky_header_enable',
        array(
            'label' => esc_html__( 'Sticky Header', 'edubin' ),
            'section' => 'main_header_section'
        )
    ) );

    // Top search icon
    $wp_customize->add_setting( 'top_search_enable',
        array(
            'default' => $this->defaults['top_search_enable'],
            'transport' => 'refresh',
            'sanitize_callback' => 'edubin_switch_sanitization'
        )
    );
    $wp_customize->add_control( new Edubin_Toggle_Switch_Custom_control( $wp_customize, 'top_search_enable',
        array(
            'label' => esc_html__( 'Search', 'edubin' ),
            'section' => 'main_header_section'
        )
    ) );  

    // Top search icon
    $wp_customize->add_setting( 'top_cart_enable',
        array(
            'default' => $this->defaults['top_cart_enable'],
            'transport' => 'refresh',
            'sanitize_callback' => 'edubin_switch_sanitization'
        )
    );
    $wp_customize->add_control( new Edubin_Toggle_Switch_Custom_control( $wp_customize, 'top_cart_enable',
        array(
            'label' => esc_html__( 'Shop Cart', 'edubin' ),
            'section' => 'main_header_section'
        )
    ) );       
    // Menu active color
    $wp_customize->add_setting( 'home_menu_acive_color',
        array(
            'default' => $this->defaults['home_menu_acive_color'],
            'transport' => 'refresh',
            'sanitize_callback' => 'edubin_switch_sanitization'
        )
    );
    $wp_customize->add_control( new Edubin_Toggle_Switch_Custom_control( $wp_customize, 'home_menu_acive_color',
        array(
            'label' => esc_html__( 'Home Menu Active Color', 'edubin' ),
            'section' => 'main_header_section'
        )
    ) );
