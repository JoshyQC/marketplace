<?php

// Primary color
$wp_customize->add_setting( 'primary_color',
    array(
        'default' => $this->defaults['primary_color'],
        'transport'         => 'refresh',
        'sanitize_callback' => 'edubin_hex_rgba_sanitization',
    )
);
$wp_customize->add_control( new Edubin_Customize_Alpha_Color_Control( $wp_customize, 'primary_color',
    array(
        'label' => esc_html__( 'Primary Color', 'edubin' ),
        'section' => 'colors',
        'show_opacity' => true,
        'priority' => 5,
        'palette' => array(
            '#000',
            '#fff',
            '#df312c',
            '#df9a23',
            '#eef000',
            '#7ed934',
            '#1571c1',
            '#8309e7'
        )
    )
) );
// Secondary color
$wp_customize->add_setting( 'secondary_color',
    array(
        'default' => $this->defaults['secondary_color'],
        'transport'         => 'refresh',
        'sanitize_callback' => 'edubin_hex_rgba_sanitization'
    )
);
$wp_customize->add_control( new Edubin_Customize_Alpha_Color_Control( $wp_customize, 'secondary_color',
    array(
        'label' => esc_html__( 'Secondary Color', 'edubin' ),
        'section' => 'colors',
        'show_opacity' => true,
        'priority' => 5,
        'palette' => array(
            '#000',
            '#fff',
            '#df312c',
            '#df9a23',
            '#eef000',
            '#7ed934',
            '#1571c1',
            '#8309e7'
        )
    )
) );


// Tertiary color
$wp_customize->add_setting( '3rd_color',
    array(
        'default' => $this->defaults['3rd_color'],
        'transport'         => 'refresh',
        'sanitize_callback' => 'edubin_hex_rgba_sanitization'
    )
);
$wp_customize->add_control( new Edubin_Customize_Alpha_Color_Control( $wp_customize, '3rd_color',
    array(
        'label' => esc_html__( 'Tertiary Color', 'edubin' ),
        'section' => 'colors',
        'show_opacity' => true,
        'priority' => 5,
        'palette' => array(
            '#000',
            '#fff',
            '#df312c',
            '#df9a23',
            '#eef000',
            '#7ed934',
            '#1571c1',
            '#8309e7'
        )
    )
) );
// Test of Hidden Control
$wp_customize->add_setting( 'link_hidden_separator',
    array(
        'default' => '',
        'transport' => 'refresh',
        'sanitize_callback' => 'edubin_text_sanitization'
    )
);
$wp_customize->add_control( 'link_hidden_separator',
    array(
        'label' => __( '', 'edubin' ),
        'section' => 'colors',
        'type' => 'hidden',
        'priority' => 5,
    )
);
// Link color
$wp_customize->add_setting( 'link_color',
    array(
        'default' => $this->defaults['link_color'],
        'transport'         => 'refresh',
        'sanitize_callback' => 'edubin_hex_rgba_sanitization'
    )
);
$wp_customize->add_control( new Edubin_Customize_Alpha_Color_Control( $wp_customize, 'link_color',
    array(
        'label' => esc_html__( 'Link Color', 'edubin' ),
        'section' => 'colors',
        'show_opacity' => true,
        'priority' => 5,
        'palette' => array(
            '#000',
            '#fff',
            '#df312c',
            '#df9a23',
            '#eef000',
            '#7ed934',
            '#1571c1',
            '#8309e7'
        )
    )
) );

// Link hover color
$wp_customize->add_setting( 'link_hover_color',
    array(
        'default' => $this->defaults['link_hover_color'],
        'transport'         => 'refresh',
        'sanitize_callback' => 'edubin_hex_rgba_sanitization'
    )
);
$wp_customize->add_control( new Edubin_Customize_Alpha_Color_Control( $wp_customize, 'link_hover_color',
    array(
        'label' => esc_html__( 'Link Hover Color', 'edubin' ),
        'section' => 'colors',
        'show_opacity' => true,
        'priority' => 5,
        'palette' => array(
            '#000',
            '#fff',
            '#df312c',
            '#df9a23',
            '#eef000',
            '#7ed934',
            '#1571c1',
            '#8309e7'
        )
    )
) );

// Test of Hidden Control
$wp_customize->add_setting( 'button_hidden_separator',
    array(
        'default' => '',
        'transport' => 'refresh',
        'sanitize_callback' => 'edubin_text_sanitization'
    )
);
$wp_customize->add_control( 'button_hidden_separator',
    array(
        'label' => __( '', 'edubin' ),
        'section' => 'colors',
        'type' => 'hidden',
        'priority' => 5,
    )
);
// Button/Submit color
$wp_customize->add_setting( 'btn_color',
    array(
        'default' => $this->defaults['btn_color'],
        'transport'         => 'refresh',
        'sanitize_callback' => 'edubin_hex_rgba_sanitization'
    )
);
$wp_customize->add_control( new Edubin_Customize_Alpha_Color_Control( $wp_customize, 'btn_color',
    array(
        'label' => esc_html__( 'Button Color', 'edubin' ),
        'section' => 'colors',
        'show_opacity' => true,
        'priority' => 5,
        'palette' => array(
            '#000',
            '#fff',
            '#df312c',
            '#df9a23',
            '#eef000',
            '#7ed934',
            '#1571c1',
            '#8309e7'
        )
    )
) );


// Button/Submit hover color
$wp_customize->add_setting( 'btn_hover_color',
    array(
        'default' => $this->defaults['btn_hover_color'],
        'transport'         => 'refresh',
        'sanitize_callback' => 'edubin_hex_rgba_sanitization'
    )
);
$wp_customize->add_control( new Edubin_Customize_Alpha_Color_Control( $wp_customize, 'btn_hover_color',
    array(
        'label' => esc_html__( 'Button Hover Color', 'edubin' ),
        'section' => 'colors',
        'show_opacity' => true,
        'priority' => 5,
        'palette' => array(
            '#000',
            '#fff',
            '#df312c',
            '#df9a23',
            '#eef000',
            '#7ed934',
            '#1571c1',
            '#8309e7'
        )
    )
) );
// Test of Hidden Control
$wp_customize->add_setting( 'menu_hidden_separator',
    array(
        'default' => '',
        'transport' => 'refresh',
        'sanitize_callback' => 'edubin_text_sanitization'
    )
);
$wp_customize->add_control( 'menu_hidden_separator',
    array(
        'label' => __( '', 'edubin' ),
        'section' => 'colors',
        'type' => 'hidden',
        'priority' => 5,
    )
);
// Menu
$wp_customize->add_setting( 'menu_text_color',
    array(
        'default' => $this->defaults['menu_text_color'],
        'transport'         => 'refresh',
        'sanitize_callback' => 'edubin_hex_rgba_sanitization'
    )
);
$wp_customize->add_control( new Edubin_Customize_Alpha_Color_Control( $wp_customize, 'menu_text_color',
    array(
        'label' => esc_html__( 'Menu Text Color', 'edubin' ),
        'section' => 'colors',
        'show_opacity' => true,
        'priority' => 5,
        'palette' => array(
            '#000',
            '#fff',
            '#df312c',
            '#df9a23',
            '#eef000',
            '#7ed934',
            '#1571c1',
            '#8309e7'
        )
    )
) );
// menu link/hover color
$wp_customize->add_setting( 'menu_hover_color',
    array(
        'default' => $this->defaults['menu_hover_color'],
        'transport'         => 'refresh',
        'sanitize_callback' => 'edubin_hex_rgba_sanitization'
    )
);
$wp_customize->add_control( new Edubin_Customize_Alpha_Color_Control( $wp_customize, 'menu_hover_color',
    array(
        'label' => esc_html__( 'Menu Active/Hover Color', 'edubin' ),
        'section' => 'colors',
        'show_opacity' => true,
        'priority' => 5,
        'palette' => array(
            '#000',
            '#fff',
            '#df312c',
            '#df9a23',
            '#eef000',
            '#7ed934',
            '#1571c1',
            '#8309e7'
        )
    )
) );

// Sub menu color
$wp_customize->add_setting( 'sub_menu_text_color',
    array(
        'default' => $this->defaults['sub_menu_text_color'],
        'transport'         => 'refresh',
        'sanitize_callback' => 'edubin_hex_rgba_sanitization'
    )
);
$wp_customize->add_control( new Edubin_Customize_Alpha_Color_Control( $wp_customize, 'sub_menu_text_color',
    array(
        'label' => esc_html__( 'Sub Menu Text Color', 'edubin' ),
        'section' => 'colors',
        'show_opacity' => true,
        'priority' => 5,
        'palette' => array(
            '#000',
            '#fff',
            '#df312c',
            '#df9a23',
            '#eef000',
            '#7ed934',
            '#1571c1',
            '#8309e7'
        )
    )
) );


// Sub menu arrow color
$wp_customize->add_setting( 'sub_menu_arrow_color',
    array(
        'default' => $this->defaults['sub_menu_arrow_color'],
        'transport'         => 'refresh',
        'sanitize_callback' => 'edubin_hex_rgba_sanitization'
    )
);
$wp_customize->add_control( new Edubin_Customize_Alpha_Color_Control( $wp_customize, 'sub_menu_arrow_color',
    array(
        'label' => esc_html__( 'Sub Menu Arrow Color', 'edubin' ),
        'section' => 'colors',
        'show_opacity' => true,
        'priority' => 5,
        'palette' => array(
            '#000',
            '#fff',
            '#df312c',
            '#df9a23',
            '#eef000',
            '#7ed934',
            '#1571c1',
            '#8309e7'
        )
    )
) );

// Sub menu bg color
$wp_customize->add_setting( 'sub_menu_bg_color',
    array(
        'default' => $this->defaults['sub_menu_bg_color'],
        'transport'         => 'refresh',
        'sanitize_callback' => 'edubin_hex_rgba_sanitization'
    )
);
$wp_customize->add_control( new Edubin_Customize_Alpha_Color_Control( $wp_customize, 'sub_menu_bg_color',
    array(
        'label' => esc_html__( 'Sub Menu Background Color', 'edubin' ),
        'section' => 'colors',
        'show_opacity' => true,
        'priority' => 5,
        'palette' => array(
            '#000',
            '#fff',
            '#df312c',
            '#df9a23',
            '#eef000',
            '#7ed934',
            '#1571c1',
            '#8309e7'
        )
    )
) );

// Sub menu border color
$wp_customize->add_setting( 'sub_menu_border_color',
    array(
        'default' => $this->defaults['sub_menu_border_color'],
        'transport'         => 'refresh',
        'sanitize_callback' => 'edubin_hex_rgba_sanitization'
    )
);
$wp_customize->add_control( new Edubin_Customize_Alpha_Color_Control( $wp_customize, 'sub_menu_border_color',
    array(
        'label' => esc_html__( 'Sub Menu Border Color', 'edubin' ),
        'section' => 'colors',
        'show_opacity' => true,
        'priority' => 5,
        'palette' => array(
            '#000',
            '#fff',
            '#df312c',
            '#df9a23',
            '#eef000',
            '#7ed934',
            '#1571c1',
            '#8309e7'
        )
    )
) );
// Mobile menu icon color
$wp_customize->add_setting( 'mobile_menu_icon_color',
    array(
        'default' => $this->defaults['mobile_menu_icon_color'],
        'transport'         => 'refresh',
        'sanitize_callback' => 'edubin_hex_rgba_sanitization'
    )
);
$wp_customize->add_control( new Edubin_Customize_Alpha_Color_Control( $wp_customize, 'mobile_menu_icon_color',
    array(
        'label' => esc_html__( 'Mobile Menu Icon Color', 'edubin' ),
        'section' => 'colors',
        'show_opacity' => true,
        'priority' => 5,
        'palette' => array(
            '#000',
            '#fff',
            '#df312c',
            '#df9a23',
            '#eef000',
            '#7ed934',
            '#1571c1',
            '#8309e7'
        )
    )
) );
// Test of Hidden Control
$wp_customize->add_setting( 'other_hidden_separator',
    array(
        'default' => '',
        'transport' => 'refresh',
        'sanitize_callback' => 'edubin_text_sanitization'
    )
);
$wp_customize->add_control( 'other_hidden_separator',
    array(
        'label' => __( '', 'edubin' ),
        'section' => 'colors',
        'type' => 'hidden',
        'priority' => 5,
    )
);
