<?php
// Logo
$wp_customize->add_setting( 'logo_size',
    array(
        'default' => $this->defaults['logo_size'],
        'transport' => 'refresh',
        'sanitize_callback' => 'edubin_sanitize_integer'

    )
);
$wp_customize->add_control( new Edubin_Slider_Custom_Control( $wp_customize, 'logo_size',
    array(
        'label' => esc_html__( 'Logo Size', 'edubin' ),
        'section' => 'title_tagline', 
        'priority'       => 9,
        'input_attrs' => array(
            'min' => 10, 
            'max' => 400, 
            'step' => 1, 
        ),
    )
) );

// Preloader
$wp_customize->add_section('preloader_section',
    array(
        'title' => esc_html__('Preloader', 'edubin'),
        'panel' => 'general_panel',
        'priority'   => 50,
    )
);
$wp_customize->add_setting('preloader_show',
    array(
        'default'           => $this->defaults['preloader_show'],
        'transport'         => 'refresh',
        'sanitize_callback' => 'edubin_switch_sanitization',
    )
);
$wp_customize->add_control(new Edubin_Toggle_Switch_Custom_control($wp_customize, 'preloader_show',
    array(
        'label'   => esc_html__('Preloader', 'edubin'),
        'section' => 'preloader_section',
    )
));

// Back to Top;
$wp_customize->add_section('back_to_top_section',
    array(
        'title' => esc_html__('Back to Top', 'edubin'),
        'panel' => 'general_panel',
        'priority'   => 50,
    )
);
$wp_customize->add_setting('back_to_top_show',
    array(
        'default'           => $this->defaults['back_to_top_show'],
        'transport'         => 'refresh',
        'sanitize_callback' => 'edubin_switch_sanitization',
    )
);
$wp_customize->add_control(new Edubin_Toggle_Switch_Custom_control($wp_customize, 'back_to_top_show',
    array(
        'label'   => esc_html__('Back to Top', 'edubin'),
        'section' => 'back_to_top_section',
    )
));

// Breadcrumb
$wp_customize->add_section('breadcrumb_section',
    array(
        'title' => esc_html__('Breadcrumb', 'edubin'),
        'panel' => 'general_panel',
        'priority'   => 50,
    )
);

$wp_customize->add_setting('breadcrumb_show',
    array(
        'default'           => $this->defaults['breadcrumb_show'],
        'transport'         => 'refresh',
        'sanitize_callback' => 'edubin_switch_sanitization',
    )
);
$wp_customize->add_control(new Edubin_Toggle_Switch_Custom_control($wp_customize, 'breadcrumb_show',
    array(
        'label'   => esc_html__('Breadcrumb', 'edubin'),
        'section' => 'breadcrumb_section',
    )
));

// 404 page
$wp_customize->add_setting('error_404_img', array(
    'capability'        => 'edit_theme_options',
    'sanitize_callback' => 'esc_url_raw',
    'transport' => 'refresh',
    'default'           => $this->defaults['error_404_img'],
));

$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'error_404_img', array(
    'label'    => esc_html__('404 Image', 'edubin'),
    'section'  => 'error_404',
    'settings' => 'error_404_img',
    'priority' => 9,
)));

$wp_customize->add_setting( 'error_404_heading',
    array(
        'default' => $this->defaults['error_404_heading'],
        'transport' => 'refresh',
        'sanitize_callback' => 'wp_filter_nohtml_kses'
    )
);

$wp_customize->add_control( 'error_404_heading',
    array(
        'label' => esc_html__( '404 Title', 'edubin' ),
        'type' => 'text',
        'section' => 'error_404'
    )
);
$wp_customize->add_setting( 'error_404_text',
    array(
        'default' => $this->defaults['error_404_text'],
        'transport' => 'refresh',
        'sanitize_callback' => 'wp_filter_nohtml_kses'
    )
);

$wp_customize->add_control( 'error_404_text',
    array(
        'label' => esc_html__( '404 Text', 'edubin' ),
        'type' => 'textarea',
        'section' => 'error_404'
    )
);
$wp_customize->add_setting( 'error_404_link_text',
    array(
        'default' => $this->defaults['error_404_link_text'],
        'transport' => 'refresh',
        'sanitize_callback' => 'wp_filter_nohtml_kses'
    )
);

$wp_customize->add_control( 'error_404_link_text',
    array(
        'label' => esc_html__( 'Go Home Text', 'edubin' ),
        'type' => 'text',
        'section' => 'error_404'
    )
);
