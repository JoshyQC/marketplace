<?php
    // Footer widget 
    $wp_customize->add_section( 'footer_section',
        array(
            'title' => esc_html__( 'Widget Area', 'edubin' ),
            'panel' => 'footer_panel'
        )
    );
    // Text color
    $wp_customize->add_setting( 'footer_text_color',
        array(
            'default' => $this->defaults['footer_text_color'],
            'transport'         => 'refresh',
            'sanitize_callback' => 'edubin_hex_rgba_sanitization',
        )
    );
    $wp_customize->add_control( new Edubin_Customize_Alpha_Color_Control( $wp_customize, 'footer_text_color',
        array(
            'label' => esc_html__( 'Text Color', 'edubin' ),
            'section' => 'footer_section',
            'show_opacity' => true,
            'palette' => array(
                '#000',
                '#fff',
                '#df312c',
                '#df9a23',
                '#eef000',
                '#7ed934',
                '#1571c1',
                '#8309e7'
            )
        )
    ) );

    // Link color
    $wp_customize->add_setting( 'footer_link_color',
        array(
            'default' => $this->defaults['footer_link_color'],
            'transport'         => 'refresh',
            'sanitize_callback' => 'edubin_hex_rgba_sanitization',
        )
    );
    $wp_customize->add_control( new Edubin_Customize_Alpha_Color_Control( $wp_customize, 'footer_link_color',
        array(
            'label' => esc_html__( 'Link Color', 'edubin' ),
            'section' => 'footer_section',
            'show_opacity' => true,
            'palette' => array(
                '#000',
                '#fff',
                '#df312c',
                '#df9a23',
                '#eef000',
                '#7ed934',
                '#1571c1',
                '#8309e7'
            )
        )
    ) );
    // Button/footer color
    $wp_customize->add_setting( 'footer_btn_submit_color',
        array(
            'default' => $this->defaults['footer_btn_submit_color'],
            'transport'         => 'refresh',
            'sanitize_callback' => 'edubin_hex_rgba_sanitization',
        )
    );
    $wp_customize->add_control( new Edubin_Customize_Alpha_Color_Control( $wp_customize, 'footer_btn_submit_color',
        array(
            'label' => esc_html__( 'Button/Submit', 'edubin' ),
            'section' => 'footer_section',
            'show_opacity' => true,
            'palette' => array(
                '#000',
                '#fff',
                '#df312c',
                '#df9a23',
                '#eef000',
                '#7ed934',
                '#1571c1',
                '#8309e7'
            )
        )
    ) );

    // Background color
    $wp_customize->add_setting( 'footer_bg_color',
        array(
            'default' => $this->defaults['footer_bg_color'],
            'transport'         => 'refresh',
            'sanitize_callback' => 'edubin_hex_rgba_sanitization',
        )
    );
    $wp_customize->add_control( new Edubin_Customize_Alpha_Color_Control( $wp_customize, 'footer_bg_color',
        array(
            'label' => esc_html__( 'Background Color', 'edubin' ),
            'section' => 'footer_section',
            'show_opacity' => true,
            'palette' => array(
                '#000',
                '#fff',
                '#df312c',
                '#df9a23',
                '#eef000',
                '#7ed934',
                '#1571c1',
                '#8309e7'
            )
        )
    ) );
    
    // Copyright 
    $wp_customize->add_section( 'copyright_section',
        array(
            'title' => esc_html__( 'Copyright', 'edubin' ),
            'panel' => 'footer_panel'
        )
    );
    $wp_customize->add_setting( 'copyright_text',
        array(
            'default' => $this->defaults['copyright_text'],
            'transport' => 'refresh',
            'sanitize_callback' => 'wp_filter_nohtml_kses'
        )
    );

    $wp_customize->add_control( 'copyright_text',
        array(
            'label' => esc_html__( 'Copyright', 'edubin' ),
            'type' => 'text',
            'section' => 'copyright_section'
        )
    );
 // Text color
    $wp_customize->add_setting( 'copyright_text_color',
        array(
            'default' => $this->defaults['copyright_text_color'],
            'transport'         => 'refresh',
            'sanitize_callback' => 'edubin_hex_rgba_sanitization',
        )
    );
    $wp_customize->add_control( new Edubin_Customize_Alpha_Color_Control( $wp_customize, 'copyright_text_color',
        array(
            'label' => esc_html__( 'Text Color', 'edubin' ),
            'section' => 'copyright_section',
            'show_opacity' => true,
            'palette' => array(
                '#000',
                '#fff',
                '#df312c',
                '#df9a23',
                '#eef000',
                '#7ed934',
                '#1571c1',
                '#8309e7'
            )
        )
    ) );

    // Link color
    $wp_customize->add_setting( 'copyright_link_color',
        array(
            'default' => $this->defaults['copyright_link_color'],
            'transport'         => 'refresh',
            'sanitize_callback' => 'edubin_hex_rgba_sanitization',
        )
    );
    $wp_customize->add_control( new Edubin_Customize_Alpha_Color_Control( $wp_customize, 'copyright_link_color',
        array(
            'label' => esc_html__( 'Link Color', 'edubin' ),
            'section' => 'copyright_section',
            'show_opacity' => true,
            'palette' => array(
                '#000',
                '#fff',
                '#df312c',
                '#df9a23',
                '#eef000',
                '#7ed934',
                '#1571c1',
                '#8309e7'
            )
        )
    ) );
    // Background color
    $wp_customize->add_setting( 'copyright_bg_color',
        array(
            'default' => $this->defaults['copyright_bg_color'],
            'transport'         => 'refresh',
            'sanitize_callback' => 'edubin_hex_rgba_sanitization',
        )
    );
    $wp_customize->add_control( new Edubin_Customize_Alpha_Color_Control( $wp_customize, 'copyright_bg_color',
        array(
            'label' => esc_html__( 'Background Color', 'edubin' ),
            'section' => 'copyright_section',
            'show_opacity' => true,
            'palette' => array(
                '#000',
                '#fff',
                '#df312c',
                '#df9a23',
                '#eef000',
                '#7ed934',
                '#1571c1',
                '#8309e7'
            )
        )
    ) );

    // Footer widget area 
    $wp_customize->add_setting( 'footer_widget_area_column',
        array(
            'default' => $this->defaults['footer_widget_area_column'],
            'transport' => 'refresh',
            'sanitize_callback' => 'edubin_radio_sanitization'
        )
    );
    $wp_customize->add_control( new Edubin_Image_Radio_Button_Custom_Control( $wp_customize, 'footer_widget_area_column',
        array(
            'label' => esc_html__( 'Footer Widget Column', 'edubin' ),
            'description' => esc_html__( 'Select your footer widget column', 'edubin' ),
            'section' => 'footer_section',
            'choices' => array(
                '12' => array(
                    'image' => trailingslashit( get_template_directory_uri() ) . 'admin/assets/images/footer-1.png',
                    'name' => esc_html__( '12', 'edubin' )
                ),
                '6_6' => array(
                    'image' => trailingslashit( get_template_directory_uri() ) . 'admin/assets/images/footer-2.png',
                    'name' => esc_html__( '6-6', 'edubin' )
                ),
                '4_4_4' => array(
                    'image' => trailingslashit( get_template_directory_uri() ) . 'admin/assets/images/footer-3.png',
                    'name' => esc_html__( '4-4-4', 'edubin' )
                ),
                '3_3_3_3' => array(
                    'image' => trailingslashit( get_template_directory_uri() ) . 'admin/assets/images/footer-4.png',
                    'name' => esc_html__( '3-3-3-3', 'edubin' )
                ),
                '3_6_3' => array(
                    'image' => trailingslashit( get_template_directory_uri() ) . 'admin/assets/images/footer-5.png',
                    'name' => esc_html__( '3-6-3', 'edubin' )
                ),
                '4_3_2_3' => array(
                    'image' => trailingslashit( get_template_directory_uri() ) . 'admin/assets/images/footer-6.png',
                    'name' => esc_html__( '4-3-2-3', 'edubin' )
                ),
            )
        )
    ) );