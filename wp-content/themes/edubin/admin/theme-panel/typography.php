<?php
$wp_customize->add_section( 'typography_section',
    array(
        'title' => esc_html__( 'Typography', 'edubin' ),
        'panel' => 'general_panel',
        'priority'   => 40,
    )
);
$font_choices = array(
    'Source Sans Pro:400,700,400italic,700italic' => 'Source Sans Pro',
    'Open Sans:400italic,700italic,400,700' => 'Open Sans',
    'Oswald:400,700' => 'Oswald',
    'Playfair Display:400,700,400italic' => 'Playfair Display',
    'Montserrat:400,700' => 'Montserrat',
    'Raleway:400,700' => 'Raleway',
    'Droid Sans:400,700' => 'Droid Sans',
    'Lato:400,700,400italic,700italic' => 'Lato',
    'Arvo:400,700,400italic,700italic' => 'Arvo',
    'Lora:400,700,400italic,700italic' => 'Lora',
    'Merriweather:400,300italic,300,400italic,700,700italic' => 'Merriweather',
    'Oxygen:400,300,700' => 'Oxygen',
    'PT Serif:400,700' => 'PT Serif',
    'PT Sans:400,700,400italic,700italic' => 'PT Sans',
    'PT Sans Narrow:400,700' => 'PT Sans Narrow',
    'Cabin:400,700,400italic' => 'Cabin',
    'Fjalla One:400' => 'Fjalla One',
    'Francois One:400' => 'Francois One',
    'Josefin Sans:400,300,600,700' => 'Josefin Sans',
    'Libre Baskerville:400,400italic,700' => 'Libre Baskerville',
    'Arimo:400,700,400italic,700italic' => 'Arimo',
    'Ubuntu:400,700,400italic,700italic' => 'Ubuntu',
    'Bitter:400,700,400italic' => 'Bitter',
    'Droid Serif:400,700,400italic,700italic' => 'Droid Serif',
    'Roboto:400,400italic,700,700italic' => 'Roboto',
    'Open Sans Condensed:700,300italic,300' => 'Open Sans Condensed',
    'Roboto Condensed:400italic,700italic,400,700' => 'Roboto Condensed',
    'Roboto Slab:400,700' => 'Roboto Slab',
    'Yanone Kaffeesatz:400,700' => 'Yanone Kaffeesatz',
    'Rokkitt:400' => 'Rokkitt',
);

$wp_customize->add_setting( 'body_fonts', array(
        'sanitize_callback' => 'edubin_sanitize_fonts',
        'default' => $this->defaults['body_fonts'],
    )
);
$wp_customize->add_control( 'body_fonts', array(
        'type' => 'select',
        'label' => esc_html__( 'Body Fonts', 'edubin' ),
        'section' => 'typography_section',
        'choices' => $font_choices
    )
);

$wp_customize->add_setting( 'headings_fonts', array(
        'sanitize_callback' => 'edubin_sanitize_fonts',
        'default' => $this->defaults['headings_fonts'],
    )
);
$wp_customize->add_control( 'headings_fonts', array(
        'type' => 'select',
        'label' => esc_html__( 'Heading Fonts', 'edubin' ),
        'section' => 'typography_section',
        'choices' => $font_choices
    )
);

$wp_customize->add_setting( 'menu_font', array(
        'sanitize_callback' => 'edubin_sanitize_fonts',
        'default' => $this->defaults['menu_font'],
    )
);
$wp_customize->add_control( 'menu_font', array(
        'type' => 'select',
        'label' => esc_html__( 'Menu Fonts', 'edubin' ),
        'section' => 'typography_section',
        'choices' => $font_choices
    )
);

