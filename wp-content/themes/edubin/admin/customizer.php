<?php

/**
 * Sanitize the colorscheme.
 *
 * @param string $input Color scheme.
 */
function edubin_sanitize_colorscheme($input)
{
    $valid = array('light', 'dark', 'custom');

    if (in_array($input, $valid, true)) {
        return $input;
    }

    return 'light';
}

/**
 * Render the site title for the selective refresh partial.
 *
 * @since Edubin 1.0
 * @see edubin_customize_register()
 *
 * @return void
 */
function edubin_customize_partial_blogname()
{
    bloginfo('name');
};

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @since Edubin 1.0
 * @see edubin_customize_register()
 *
 * @return void
 */
function edubin_customize_partial_blogdescription()
{
    bloginfo('description');
}

/**
 * Return whether we're previewing the front page and it's a static page.
 */
function edubin_is_static_front_page()
{
    return (is_front_page() && !is_home());
}

/**
 * Return whether we're on a view that supports a one or two column layout.
 */
function edubin_is_view_with_layout_option()
{
    // This option is available on all pages. It's also available on archives when there isn't a sidebar.
    return (is_page() || (is_archive() && !is_active_sidebar('sidebar-1')));
}

if (!function_exists('edubin_sanitize_checkbox')):

    /**
     * Sanitize checkbox.
     *
     * @since 1.0.0
     *
     * @param bool $checked Whether the checkbox is checked.
     * @return bool Whether the checkbox is checked.
     */
    function edubin_sanitize_checkbox($checked)
{

        return ((isset($checked) && true === $checked) ? true : false);

    }

endif;

if (!function_exists('edubin_sanitize_select')):

    /**
     * Sanitize select.
     *
     * @since 1.0.0
     *
     * @param mixed                $input The value to sanitize.
     * @param WP_Customize_Setting $setting WP_Customize_Setting instance.
     * @return mixed Sanitized value.
     */
    function edubin_sanitize_select($input, $setting)
{

        // Ensure input is clean.
        $input = sanitize_text_field($input);

        // Get list of choices from the control associated with the setting.
        $choices = $setting->manager->get_control($setting->id)->choices;

        // If the input is a valid key, return it; otherwise, return the default.
        return (array_key_exists($input, $choices) ? $input : $setting->default);

    }

endif;

// If custom color active
if (!function_exists('edubin_custom_color_active')):

    function edubin_custom_color_active($control)
{

        if (true == $control->manager->get_setting('show_custom_color')->value()) {
            return true;
        } else {
            return false;
        }

    }
endif;

if (!function_exists('edubin_header_style')):
/**
 * Styles the header image and text displayed on the blog.
 *
 * @see edubin_custom_header_setup().
 */
    function edubin_header_style()
{

        $header_text_color = get_header_textcolor();?>
      <?php if (!empty($header_text_color && $header_text_color !== 'blank')): ?>
            <style type="text/css">
                .site-description{
                     color: #<?php echo esc_attr($header_text_color); ?>;
                }
            </style>
        <?php endif;?>
<?php }

endif;

/**
 * Customize video play/pause button in the custom header.
 *
 * @param array $settings Video settings.
 * @return array The filtered video settings.
 */
function edubin_video_controls($settings)
{
    $settings['l10n']['play']  = '<span class="screen-reader-text">' . esc_html__('Play background video', 'edubin') . '</span>' . edubin_get_svg(array('icon' => 'play'));
    $settings['l10n']['pause'] = '<span class="screen-reader-text">' . esc_html__('Pause background video', 'edubin') . '</span>' . edubin_get_svg(array('icon' => 'pause'));
    return $settings;
}
add_filter('header_video_settings', 'edubin_video_controls');

// Custom Theme Style
if (!function_exists('edubin_customizer_theme_style')):
    function edubin_customizer_theme_style(){

        $defaults = edubin_generate_defaults();
        $logo_size = get_theme_mod( 'logo_size', $defaults['logo_size']);
        $menu_text_color = get_theme_mod( 'menu_text_color', $defaults['menu_text_color']);
         
        ?>

            <?php if( $logo_size ): ?>
                <style type="text/css">
                    /* For logo only */
                    body.home.title-tagline-hidden.has-header-image .custom-logo-link img, 
                    body.home.title-tagline-hidden.has-header-video .custom-logo-link img, 
                    .header-wrapper .header-menu .site-branding img,
                     .site-branding img.custom-logo {
                        max-width: <?php echo esc_attr($logo_size.'px'); ?>;
                    }
                </style>
            <?php endif; ?>

            <?php if (get_theme_mod('home_menu_acive_color') == false) : ?>
             <!-- For menu active color only -->
                <style type="text/css">
                <?php if (get_theme_mod('home_menu_acive_color') == false && get_theme_mod('menu_text_color')) : ?>
                   .main-navigation li.menu-item-home.current-menu-item.current-menu-parent>a{
                        color: <?php echo esc_attr($menu_text_color); ?>;
                    }
                <?php elseif(get_theme_mod('home_menu_acive_color') == false) : ?>
                   .main-navigation li.menu-item-home.current-menu-item.current-menu-parent>a{
                        color: #07294d;
                    }
                <?php endif; ?>
                </style>
            <?php endif; ?>

            <?php if (!empty(get_theme_mod('primary_color') || get_theme_mod('secondary_color') || get_theme_mod('3rd_color') || get_theme_mod('link_color') || get_theme_mod('link_color') || get_theme_mod('link_hover_color') || get_theme_mod('btn_color') || get_theme_mod('btn_hover_color') || get_theme_mod('footer_bg_color')) || get_theme_mod('header_top_text_color') || get_theme_mod('header_top_bg_color') || get_theme_mod('header_top_link_color') || get_theme_mod('footer_text_color') || get_theme_mod('footer_bg_color') || get_theme_mod('footer_link_color') || get_theme_mod('footer_btn_submit_color') || get_theme_mod('menu_text_color') || get_theme_mod('menu_hover_color') || get_theme_mod('sub_menu_text_color') || get_theme_mod('sub_menu_bg_color')): 
            ?>

                <style type="text/css">
                    /*Core*/
                    .site-title, .site-title a{
                        color: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    h1, h2, h3, h4, h5, h6{
                        color: <?php echo esc_attr(get_theme_mod('3rd_color')); ?>;
                    }
                    .edubin-entry-footer .cat-links, .edubin-entry-footer .tags-links{
                        color: <?php echo esc_attr(get_theme_mod('3rd_color')); ?>;
                    }
                    .widget .widget-title{
                        color: <?php echo esc_attr(get_theme_mod('3rd_color')); ?>;
                    }
                    button, input[type="button"], input[type="submit"]{
                         background-color: <?php echo esc_attr(get_theme_mod('btn_color')); ?>;
                    }
                     button, input[type="button"],
                    input[type="submit"]{
                        color:<?php echo esc_attr(get_theme_mod('btn_hover_color')); ?>;
                    }
                    button:hover, button:focus, input[type="button"]:hover,
                    input[type="button"]:focus, input[type="submit"]:hover,
                    input[type="submit"]:focus{
                        background-color:<?php echo esc_attr(get_theme_mod('btn_hover_color')); ?>;
                    }
                    button:hover, button:focus, input[type="button"]:hover, input[type="button"]:focus, input[type="submit"]:hover, input[type="submit"]:focus{ 
                         color:<?php echo esc_attr(get_theme_mod('btn_color')); ?>;
                    }
                    .edubin-main-btn a{
                         color: <?php echo esc_attr(get_theme_mod('btn_hover_color')); ?>;
                    }
                    .edubin-main-btn:hover {
                        border-color:<?php echo esc_attr(get_theme_mod('btn_hover_color')); ?>;
                        background-color:<?php echo esc_attr(get_theme_mod('btn_hover_color')); ?>;
                    }
                    .edubin-main-btn{
                         background-color: <?php echo esc_attr(get_theme_mod('btn_color')); ?>;
                    }
                    .colors-light .pagination .nav-links .page-numbers.current{
                         background-color: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    .colors-light .pagination .nav-links a:hover{
                         background: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    .colors-light .pagination .nav-links .page-numbers.current:hover{
                         background: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    .back-to-top{
                         background: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    .preloader .color-1{
                         background: <?php echo esc_attr(get_theme_mod('primary_color')); ?> !important;
                    }
                    .site-footer .widget .edubin-social a:hover{
                         color: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    .edubin-social a.edubin-social-icon{
                        background: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                        border-color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .header-right-icon ul li a span{
                        background-color: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    .header-right-icon ul li a{
                        color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }

                    /*Blog*/
                    .post .entry-meta li{
                        color: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    .post .entry-title a:hover, .post .entry-title a:focus, .post .entry-title a:active{
                        color: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    .navigation .nav-links .nav-title:hover{
                        color: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    .comment-reply-link{
                        background: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    ul.entry-meta li i{
                        color: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    #comments .logged-in-as>a:last-child{
                        color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .edubin_recent_post .edubin_recent_post_title a:hover{
                         color: <?php echo esc_attr(get_theme_mod('link_hover_color')); ?>;
                    }
                    .page-links a, .nav-links a{
                         background-color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                         border-color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .page-links .post-page-numbers.current, .nav-links .page-numbers.current{
                         background: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                         border-color: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    .page-links a:hover, .nav-links a:hover{
                         border-color: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }

                    /*Sidebar*/
                    .widget .widget-title:before{
                         background: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    .edubin_recent_post .edubin_recent_post_title a{
                         color: <?php echo esc_attr(get_theme_mod('3rd_color')); ?>;
                    }
                    table#wp-calendar td#today{
                          background: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }

                    /*Core*/
                    .colors-light .pagination .nav-links .page-numbers.current{
                         color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .colors-light .pagination .nav-links a:hover{
                         color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .back-to-top{
                         color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .back-to-top > i{
                        color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .rubix-cube .layer{
                         background-color:<?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .page-header:before{
                         background-color:<?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .edubin-search-box{
                         background-color:<?php echo esc_attr(get_theme_mod('secondary_color')); ?>70;
                    }
                    .edubin-search-box .edubin-search-form input{
                         color:<?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                         border-color:<?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .edubin-search-box .edubin-search-form button{
                         color:<?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    /*blog*/

                    .entry-title a{
                        color: <?php echo esc_attr(get_theme_mod('3rd_color')); ?>;
                    }
                    .edubin-entry-footer .cat-tags-links a:hover{
                         border-color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                         background-color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .comment-reply-link{
                       color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .comment-author-link{
                         color: <?php echo esc_attr(get_theme_mod('link_color')); ?>
                    }
                    a.comment-reply-link:hover{
                       background-color: <?php echo esc_attr(get_theme_mod('link_hover_color')); ?>;
                    }
                    body a:hover, body a:active{
                        color: <?php echo esc_attr(get_theme_mod('link_hover_color')); ?>
                    }
                    .widget a{
                         color: <?php echo esc_attr(get_theme_mod('link_color')); ?>;
                    }
                    input[type="text"]:focus, input[type="email"]:focus, input[type="url"]:focus, input[type="password"]:focus, input[type="search"]:focus, input[type="number"]:focus, input[type="tel"]:focus, input[type="range"]:focus, input[type="date"]:focus, input[type="month"]:focus, input[type="week"]:focus, input[type="time"]:focus, input[type="datetime"]:focus, input[type="datetime-local"]:focus, input[type="color"]:focus, textarea:focus{
                        border-color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .widget .tagcloud a:hover, .widget .tagcloud a:focus, .widget.widget_tag_cloud a:hover, .widget.widget_tag_cloud a:focus, .wp_widget_tag_cloud a:hover, .wp_widget_tag_cloud a:focus{
                         background-color: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    .widget .tag-cloud-link{
                        color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }

            <?php if (!empty(get_theme_mod('menu_text_color') || get_theme_mod('menu_hover_color') || get_theme_mod('sub_menu_arrow_color') || get_theme_mod('sub_menu_border_color') || get_theme_mod('mobile_menu_icon_color') || get_theme_mod('sub_menu_text_color'))): ?>
                    /*menu*/
                    .header-menu .mobile-menu-icon i{
                        color: <?php echo esc_attr(get_theme_mod('mobile_menu_icon_color')); ?>;
                    }
                     .header-menu span.zmm-dropdown-toggle{
                        color: <?php echo esc_attr(get_theme_mod('mobile_menu_icon_color')); ?>;
                    }
                    .main-navigation a{
                         color: <?php echo esc_attr(get_theme_mod('menu_text_color')); ?>;
                    }
                    .mobile-menu>ul li a{
                         color: <?php echo esc_attr(get_theme_mod('menu_text_color')); ?>;
                    }
                    .menu-effect-2 .main-navigation .current-menu-item.menu-item-home>a{
                        color: <?php echo esc_attr(get_theme_mod('menu_text_color')); ?>;
                    }
                    .menu-effect-2 .main-navigation ul ul{
                        border-top-color: <?php echo esc_attr(get_theme_mod('sub_menu_border_color')); ?>;
                    }
                    .menu-effect-2 .main-navigation li.current-menu-ancestor>a{
                        color: <?php echo esc_attr(get_theme_mod('menu_hover_color')); ?>;
                    }
                    .menu-effect-2 .main-navigation li.current-menu-item>a{
                         color: <?php echo esc_attr(get_theme_mod('menu_hover_color')); ?>;
                    }
                    .menu-effect-2 .main-navigation li ul .current-menu-item a{
                         color: <?php echo esc_attr(get_theme_mod('menu_hover_color')); ?>;
                    }
                    .menu-effect-2 .main-navigation a:hover{
                         color: <?php echo esc_attr(get_theme_mod('menu_hover_color')); ?>;
                    }
                    .menu-effect-2 .main-navigation ul ul a{
                        color: <?php echo esc_attr(get_theme_mod('sub_menu_text_color')); ?>;
                    }
                    .menu-effect-2 .main-navigation ul ul a:hover{
                         color: <?php echo esc_attr(get_theme_mod('sub_menu_text_color')); ?>;
                    }
                    .main-navigation ul ul{
                         background: <?php echo esc_attr(get_theme_mod('sub_menu_bg_color')); ?>;
                    }
                    .menu-effect-2 .main-navigation ul ul{
                           background: <?php echo esc_attr(get_theme_mod('sub_menu_bg_color')); ?>;
                    }
                    .main-navigation li.current-menu-ancestor>a{
                         color: <?php echo esc_attr(get_theme_mod('menu_hover_color')); ?>;
                    }
                    .main-navigation ul ul a::before{
                         background: <?php echo esc_attr(get_theme_mod('sub_menu_arrow_color')); ?>;
                    }
                    .main-navigation ul ul a:hover{
                         color: <?php echo esc_attr(get_theme_mod('menu_hover_color')); ?>;
                    }

            <?php endif; ?>


            <?php if (function_exists('wpforms')): //  exists for wpfroms ?>
                    .edubin-from1 div.wpforms-container-full .wpforms-form input[type=submit], .edubin-from1 div.wpforms-container-full .wpforms-form button[type=submit], .edubin-from1 div.wpforms-container-full .wpforms-form .wpforms-page-button{
                        background-color: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    .edubin-from1 div.wpforms-container-full .wpforms-form input[type=date]:focus, .edubin-from1 div.wpforms-container-full .wpforms-form input[type=datetime]:focus, .edubin-from1 div.wpforms-container-full .wpforms-form input[type=datetime-local]:focus, .edubin-from1 div.wpforms-container-full .wpforms-form input[type=email]:focus, .edubin-from1 div.wpforms-container-full .wpforms-form input[type=month]:focus, .edubin-from1 div.wpforms-container-full .wpforms-form input[type=number]:focus, .edubin-from1 div.wpforms-container-full .wpforms-form input[type=password]:focus, .edubin-from1 div.wpforms-container-full .wpforms-form input[type=range]:focus, .edubin-from1 div.wpforms-container-full .wpforms-form input[type=search]:focus, .edubin-from1 div.wpforms-container-full .wpforms-form input[type=tel]:focus, .edubin-from1 div.wpforms-container-full .wpforms-form input[type=text]:focus, .edubin-from1 div.wpforms-container-full .wpforms-form input[type=time]:focus, .edubin-from1 div.wpforms-container-full .wpforms-form input[type=url]:focus, .edubin-from1 div.wpforms-container-full .wpforms-form input[type=week]:focus, .edubin-from1 div.wpforms-container-full .wpforms-form select:focus, .edubin-from1 div.wpforms-container-full .wpforms-form textarea:focus{
                        border-color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .edubin-from1 div.wpforms-container-full .wpforms-form input[type=submit]:hover, .edubin-from1 div.wpforms-container-full .wpforms-form button[type=submit]:hover, .edubin-from1 div.wpforms-container-full .wpforms-form .wpforms-page-button:hover{
                         background-color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .edubin-from1 div.wpforms-container-full .wpforms-form input[type=submit], .edubin-from1 div.wpforms-container-full .wpforms-form button[type=submit], .edubin-from1 div.wpforms-container-full .wpforms-form .wpforms-page-button{
                        color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                <?php endif;?>

            <?php if (!empty(get_theme_mod('header_top_text_color') || get_theme_mod('header_top_link_color') || get_theme_mod('header_top_bg_color'))): ?>
                /*Header top*/
               .header-top ul li{
                    color: <?php echo esc_attr(get_theme_mod('header_top_text_color')); ?>;
                }
                .header-top ul li a{
                    color: <?php echo esc_attr(get_theme_mod('header_top_text_color')); ?>;
                }
                 .header-top{
                    background-color: <?php echo esc_attr(get_theme_mod('header_top_bg_color')); ?>;
                }
                 .header-top ul li a:hover{
                    color: <?php echo esc_attr(get_theme_mod('header_top_link_color')); ?>;
                }
                 .header-top .header-right .login-register ul li a{
                    color: <?php echo esc_attr(get_theme_mod('header_top_link_color')); ?>;
                }
            <?php endif;?>

            <?php if (!empty(get_theme_mod('footer_text_color') || get_theme_mod('footer_bg_color') || get_theme_mod('footer_link_color')) || get_theme_mod('footer_btn_submit_color')): ?>
                /*Footer*/
                .site-footer .footer-top{
                    background-color: <?php echo esc_attr(get_theme_mod('footer_bg_color')); ?>;
                }
                .site-footer .widget ul li a{
                    color: <?php echo esc_attr(get_theme_mod('footer_text_color')); ?>;
                }
                .site-footer .widget a{
                    color: <?php echo esc_attr(get_theme_mod('footer_text_color')); ?>;
                }
                .site-footer .widget p{
                    color: <?php echo esc_attr(get_theme_mod('footer_text_color')); ?>;
                }
                .site-footer .widget .widget-title{
                    color: <?php echo esc_attr(get_theme_mod('footer_text_color')); ?>;
                }
                 .site-footer .edubin-quickinfo{
                    color: <?php echo esc_attr(get_theme_mod('footer_text_color')); ?>;
                }
                .site-footer .widget ul li{
                    color: <?php echo esc_attr(get_theme_mod('footer_text_color')); ?>;
                }
                .site-footer .widget_rss .rss-date, .site-footer .widget_rss li cite{
                    color: <?php echo esc_attr(get_theme_mod('footer_text_color')); ?>;
                }
                .site-footer .widget_calendar th, .site-footer .widget_calendar td{
                     color: <?php echo esc_attr(get_theme_mod('footer_text_color')); ?>;
                }
                .site-footer .calendar_wrap table#wp-calendar caption{
                     color: <?php echo esc_attr(get_theme_mod('footer_text_color')); ?>;
                }
                .site-footer .calendar_wrap table#wp-calendar caption{
                     color: <?php echo esc_attr(get_theme_mod('footer_text_color')); ?>;
                }
                .site-footer tr{
                    border-color: <?php echo esc_attr(get_theme_mod('footer_text_color')); ?>;
                }
                .site-footer .calendar_wrap table#wp-calendar{
                    border-color: <?php echo esc_attr(get_theme_mod('footer_text_color')); ?>;
                }
                .site-footer .calendar_wrap table#wp-calendar caption{
                     border-color: <?php echo esc_attr(get_theme_mod('footer_text_color')); ?>;
                }
                .site-footer thead th{
                     border-color: <?php echo esc_attr(get_theme_mod('footer_text_color')); ?>;
                }
                .site-footer .widget.widget_nav_menu ul li a:hover{
                    color: <?php echo esc_attr(get_theme_mod('footer_link_color')); ?>;
                }
                .site-footer .widget a:hover{
                     color: <?php echo esc_attr(get_theme_mod('footer_link_color')); ?>;
                }
                .site-footer .widget ul.menu li:before{
                    color: <?php echo esc_attr(get_theme_mod('footer_link_color')); ?>;
                }
                .colors-light .widget .tag-cloud-link{
                     background-color: <?php echo esc_attr(get_theme_mod('footer_btn_submit_color')); ?>;
                }
                .site-footer button,
                .site-footer input[type="button"],
                .site-footer input[type="submit"]{
                    background-color: <?php echo esc_attr(get_theme_mod('footer_btn_submit_color')); ?>;
                }
            <?php endif;?>

            <?php if (!empty(get_theme_mod('copyright_text_color') || get_theme_mod('copyright_link_color') || get_theme_mod('copyright_bg_color'))): ?>
                /*Copyright*/
               .site-footer .site-info a{
                    color: <?php echo esc_attr(get_theme_mod('copyright_text_color')); ?>;
                }
               .site-footer .site-info p{
                    color: <?php echo esc_attr(get_theme_mod('copyright_text_color')); ?>;
                }
                .site-footer .footer-bottom{
                    background-color: <?php echo esc_attr(get_theme_mod('copyright_bg_color')); ?>;
                }
                .site-footer .site-info a:hover{
                    color: <?php echo esc_attr(get_theme_mod('copyright_link_color')); ?>;
                }
            <?php endif;?>
            <?php if (class_exists('WooCommerce')): //  exists for wpfroms ?>
                    /*Woocommerce*/
                    .woocommerce span.onsale{
                        background: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    .woocommerce #respond input#submit,
                    .woocommerce a.button,
                    .woocommerce button.button,
                    .woocommerce input.button{
                        background-color: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                        border: 1px solid <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    .woocommerce span.onsale{
                        color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .woocommerce #respond input#submit:hover,
                    .woocommerce a.button:hover,
                    .woocommerce button.button:hover,
                    .woocommerce input.button:hover{
                        background-color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                        border: 1px solid <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .woocommerce a.added_to_cart.wc-forward{
                         background-color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .woocommerce #respond input#submit,
                    .woocommerce a.button,
                    .woocommerce button.button,
                    .woocommerce input.button{
                         color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .header-right-icon ul li a span{
                         color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .woocommerce a.edubin-cart-link:hover{
                         color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .woocommerce a.add_to_cart_button:hover{
                        color: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    .woocommerce a.edubin-cart-link{
                        border: 1px solid <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                        color: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    .woocommerce a.edubin-cart-link:hover{
                         background: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    .woocommerce h2.woocommerce-loop-product__title{
                         color: <?php echo esc_attr(get_theme_mod('3rd_color')); ?>;
                    }
                    .woocommerce div.product .woocommerce-tabs ul.tabs li a{
                         color: <?php echo esc_attr(get_theme_mod('3rd_color')); ?>;
                    }
                    .woocommerce div.product .woocommerce-tabs ul.tabs li.active a{
                         color: <?php echo esc_attr(get_theme_mod('3rd_color')); ?>;
                    }
                    .woocommerce div.product .woocommerce-tabs ul.tabs li.active a:before{
                        background: <?php echo esc_attr(get_theme_mod('3rd_color')); ?>;
                    }
                <?php endif;?>

            <?php if (class_exists('Tribe__Events__Main')): // The events calender ?>
                    .tribe-events-event-cost span{
                        background: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    .tribe-events-event-cost span{
                        border-color: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    .events-address ul li .single-address .icon i{
                        color: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                   .tribe-events-list-event-title a.tribe-event-url{
                         color: <?php echo esc_attr(get_theme_mod('3rd_color')); ?>;
                    }
                    .tribe_events-template-default .edubin-event-register-from #rtec button:hover{
                         background: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    .tribe_events-template-default .edubin-event-register-from #rtec span.rtec-already-registered-reveal a{
                        color: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    #tribe-events .tribe-events-button, #tribe-events .tribe-events-button:hover, #tribe_events_filters_wrapper input[type=submit], .tribe-events-button, .tribe-events-button.tribe-active:hover, .tribe-events-button.tribe-inactive, .tribe-events-button:hover, .tribe-events-calendar td.tribe-events-present div[id*=tribe-events-daynum-], .tribe-events-calendar td.tribe-events-present div[id*=tribe-events-daynum-] > a{
                         background: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    .events-coundwon .count-down-time .single-count .number{
                         color: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    #tribe-events .tribe-events-button, .tribe-events-button{
                         color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    #tribe-events .tribe-events-button:hover, #tribe-events .tribe-events-button:hover:hover, #tribe_events_filters_wrapper input[type=submit]:hover, .tribe-events-button:hover, .tribe-events-button.tribe-active:hover:hover, .tribe-events-button.tribe-inactive:hover, .tribe-events-button:hover:hover, .tribe-events-calendar td.tribe-events-present div[id*=tribe-events-daynum-]:hover, .tribe-events-calendar td.tribe-events-present div[id*=tribe-events-daynum-] > a:hover{
                        background: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .tribe-events-event-cost span{
                        color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    #tribe-events-content a:hover, .tribe-events-adv-list-widget .tribe-events-widget-link a:hover, .tribe-events-adv-list-widget .tribe-events-widget-link a:hover:hover, .tribe-events-back a:hover, .tribe-events-back a:hover:hover, .tribe-events-event-meta a:hover, .tribe-events-list-widget .tribe-events-widget-link a:hover, .tribe-events-list-widget .tribe-events-widget-link a:hover:hover, ul.tribe-events-sub-nav a:hover, ul.tribe-events-sub-nav a:hover:hover{
                        color: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    #tribe-events td.tribe-events-present div[id*="tribe-events-daynum-"], #tribe-events td.tribe-events-present div[id*="tribe-events-daynum-"] > a{
                         background-color: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    .tribe_events-template-default .edubin-event-register-from #rtec .rtec-register-button{
                         background-color: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    .events-right [data-overlay]::before{
                        background: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .tribe-events-calendar thead th{
                          background-color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .tribe-events-calendar td.tribe-events-past div[id*="tribe-events-daynum-"], .tribe-events-calendar td.tribe-events-past div[id*="tribe-events-daynum-"] > a{
                           background-color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .tribe-events-calendar div[id*="tribe-events-daynum-"], .tribe-events-calendar div[id*="tribe-events-daynum-"] a{
                          background-color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .events-address ul li .single-address .cont h6{
                        color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                <?php endif;?>

            <?php if (class_exists('LearnPress')): // LeanPress ?>
                    .edubin-single-course-1 .thum .edubin-course-price-1 span{
                        background-color: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    .edubin-single-course-1 .course-content .course-title a:hover{
                        color: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    .learnpress .course-features ul li i{
                        color: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    .learnpress .course-item-nav .prev a:hover, .learnpress .course-item-nav .next a:hover{
                         color: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    .edubin-related-course .single-maylike .cont ul li{
                        color: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    .edubin-related-course .single-maylike .cont h4:hover{
                         color: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    .learnpress .course-curriculum ul.curriculum-sections .section-content .course-item .course-item-meta .count-questions{
                        background: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
                    }
                    .learnpress .course-features .price-button span{
                         color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .learnpress .course-curriculum ul.curriculum-sections .section-content .course-item.item-preview .course-item-status{
                         background: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .learnpress li.course-nav.active{
                         background: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .learnpress .course-curriculum ul.curriculum-sections .section-content .course-item .course-item-meta .duration{
                         background: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .edubin-single-course-1 .thum .edubin-course-price-1 span{
                        color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .learnpress ul.learn-press-nav-tabs .course-nav a{
                        color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .courses-top-search .nav .nav-item a.active{
                        color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .edubin-related-course .single-maylike .image::before{
                        background: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>900;
                    }
                    .learnpress .lp-single-course .widget-title:before{
                        background: <?php echo esc_attr(get_theme_mod('secondary_color')); ?>;
                    }
                    .learnpress .lp-single-course .widget-title{
                         color: <?php echo esc_attr(get_theme_mod('3rd_color')); ?>;
                    }
                    .learnpress .course-author>a{
                        color: <?php echo esc_attr(get_theme_mod('3rd_color')); ?>;
                    }
                <?php endif;?>

            <?php if (class_exists('SFWD_LMS')): // LearnDash ?>

                <?php if (!empty(get_theme_mod('primary_color'))): ?>

            .learndash-wrapper .ld-item-list .ld-item-list-item.ld-is-next,
            .learndash-wrapper .wpProQuiz_content .wpProQuiz_questionListItem label:focus-within {
                border-color: <?php echo esc_attr(get_theme_mod('primary_color')); ?>;
            }
            .learndash-wrapper .ld-breadcrumbs a,
            .learndash-wrapper .ld-lesson-item.ld-is-current-lesson .ld-lesson-item-preview-heading,
            .learndash-wrapper .ld-lesson-item.ld-is-current-lesson .ld-lesson-title,
            .learndash-wrapper .ld-primary-color-hover:hover,
            .learndash-wrapper .ld-primary-color,
            .learndash-wrapper .ld-primary-color-hover:hover,
            .learndash-wrapper .ld-primary-color,
            .learndash-wrapper .ld-tabs .ld-tabs-navigation .ld-tab.ld-active,
            .learndash-wrapper .ld-button.ld-button-transparent,
            .learndash-wrapper .ld-button.ld-button-reverse,
            .learndash-wrapper .ld-icon-certificate,
            .learndash-wrapper .ld-login-modal .ld-login-modal-login .ld-modal-heading,
            #wpProQuiz_user_content a,
            .learndash-wrapper .ld-item-list .ld-item-list-item a.ld-item-name:hover {
                color: <?php echo esc_attr(get_theme_mod('primary_color')); ?>; !important;
            }


            .learndash-wrapper .ld-primary-background,
            .learndash-wrapper .ld-tabs .ld-tabs-navigation .ld-tab.ld-active:after {
                background: <?php echo esc_attr(get_theme_mod('primary_color')); ?> !important;
            }

            .learndash-wrapper .ld-course-navigation .ld-lesson-item.ld-is-current-lesson .ld-status-incomplete {
                border-color: <?php echo esc_attr(get_theme_mod('primary_color')); ?> !important;
            }


            .learndash-wrapper .ld-loading::before {
                border-top:3px solid <?php echo esc_attr(get_theme_mod('primary_color')); ?> !important;
            }

            .learndash-wrapper .wpProQuiz_reviewDiv .wpProQuiz_reviewQuestion li.wpProQuiz_reviewQuestionTarget,
            .learndash-wrapper .ld-button:hover:not(.learndash-link-previous-incomplete):not(.ld-button-transparent),
            #learndash-tooltips .ld-tooltip:after,
            #learndash-tooltips .ld-tooltip,
            .learndash-wrapper .ld-primary-background,
            .learndash-wrapper .btn-join,
            .learndash-wrapper #btn-join,
            .learndash-wrapper .ld-button:not(.ld-js-register-account):not(.learndash-link-previous-incomplete):not(.ld-button-transparent),
            .learndash-wrapper .ld-expand-button,
            .learndash-wrapper .wpProQuiz_content .wpProQuiz_button,
            .learndash-wrapper .wpProQuiz_content .wpProQuiz_button2,
            .learndash-wrapper .wpProQuiz_content a#quiz_continue_link,
            .learndash-wrapper .ld-focus .ld-focus-sidebar .ld-course-navigation-heading,
            .learndash-wrapper .ld-focus .ld-focus-sidebar .ld-focus-sidebar-trigger,
            .learndash-wrapper .ld-focus-comments .form-submit #submit,
            .learndash-wrapper .ld-login-modal input[type='submit'],
            .learndash-wrapper .ld-login-modal .ld-login-modal-register,
            .learndash-wrapper .wpProQuiz_content .wpProQuiz_certificate a.btn-blue,
            .learndash-wrapper .ld-focus .ld-focus-header .ld-user-menu .ld-user-menu-items a,
            #wpProQuiz_user_content table.wp-list-table thead th,
            #wpProQuiz_overlay_close,
            .learndash-wrapper .ld-expand-button.ld-button-alternate .ld-icon {
                background-color: <?php echo esc_attr(get_theme_mod('primary_color')); ?> !important;
            }
            .learndash-wrapper .ld-focus .ld-focus-header .ld-user-menu .ld-user-menu-items:before {
                border-bottom-color: <?php echo esc_attr(get_theme_mod('primary_color')); ?> !important;
            }
            .learndash-wrapper .ld-button.ld-button-transparent:hover {
                background: transparent !important;
            }
            .learndash-wrapper .ld-focus .ld-focus-header .sfwd-mark-complete .learndash_mark_complete_button,
            .learndash-wrapper .ld-focus .ld-focus-header #sfwd-mark-complete #learndash_mark_complete_button,
            .learndash-wrapper .ld-button.ld-button-transparent,
            .learndash-wrapper .ld-button.ld-button-alternate,
            .learndash-wrapper .ld-expand-button.ld-button-alternate {
                background-color:transparent !important;
            }
            .learndash-wrapper .ld-focus-header .ld-user-menu .ld-user-menu-items a,
            .learndash-wrapper .ld-button.ld-button-reverse:hover,
            .learndash-wrapper .ld-alert-success .ld-alert-icon.ld-icon-certificate,
            .learndash-wrapper .ld-alert-warning .ld-button:not(.learndash-link-previous-incomplete),
            .learndash-wrapper .ld-primary-background.ld-status {
                color:white !important;
            }
            .learndash-wrapper .ld-status.ld-status-unlocked {
                background-color: <?php echo learndash_hex2rgb($colors['primary'], '0.2'); ?> !important;
                color: <?php echo esc_attr(get_theme_mod('primary_color')); ?> !important;
            }

        <?php endif;?>
    <?php if (!empty(get_theme_mod('secondary_color'))): ?>

        .learndash-wrapper #quiz_continue_link,
        .learndash-wrapper .ld-secondary-background,
        .learndash-wrapper .learndash_mark_complete_button,
        .learndash-wrapper #learndash_mark_complete_button,
        .learndash-wrapper .ld-status-complete,
        .learndash-wrapper .ld-alert-success .ld-button,
        .learndash-wrapper .ld-alert-success .ld-alert-icon {
            background-color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?> !important;
        }

        .learndash-wrapper .ld-secondary-color-hover:hover,
        .learndash-wrapper .ld-secondary-color,
        .learndash-wrapper .ld-focus .ld-focus-header .sfwd-mark-complete .learndash_mark_complete_button,
        .learndash-wrapper .ld-focus .ld-focus-header #sfwd-mark-complete #learndash_mark_complete_button,
        .learndash-wrapper .ld-focus .ld-focus-header .sfwd-mark-complete:after {
            color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?> !important;
        }

        .learndash-wrapper .ld-secondary-in-progress-icon {
            border-left-color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?> !important;
            border-top-color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?> !important;
        }

        .learndash-wrapper .ld-alert-success {
            border-color: <?php echo esc_attr(get_theme_mod('secondary_color')); ?> !important;
            background-color: transparent !important;
        }

            <?php endif;?>
            <?php endif;?>


            </style>
            <?php endif;?>

        <?php }

add_action('wp_head', 'edubin_customizer_theme_style');

endif;

//========= Start customizer 2 =========

if (!function_exists('edubin_customizer_preview_scripts')) {
    function edubin_customizer_preview_scripts(){
        wp_enqueue_script('edubin-customizer-preview', trailingslashit(get_template_directory_uri()) . 'admin/assets/js/customizer-preview.js', array('customize-preview', 'jquery'));
    }
}
add_action('customize_preview_init', 'edubin_customizer_preview_scripts');

/**
 * Load dynamic logic for the customizer controls area.
 */
function edubin_panels_js(){
    wp_enqueue_script('edubin-customize-controls', trailingslashit(get_template_directory_uri()) . '/assets/js/customize-controls.js', array(), 1.0, true);
}
add_action('customize_controls_enqueue_scripts', 'edubin_panels_js');

/**
 * Set our Social Icons URLs.
 * Only needed for our sample customizer preview refresh
 *
 * @return array Multidimensional array containing social media data
 */
if (!function_exists('edubin_generate_social_urls')) {
    function edubin_generate_social_urls(){
        $social_icons = array(
            array('url' => 'behance.net', 'icon' => 'flaticon-behance-logo', 'title' => esc_html__('Follow me on Behance', 'edubin'), 'class' => 'behance'),
            array('url' => 'bitbucket.org', 'icon' => 'flaticon-bitbucket-logo', 'title' => esc_html__('Fork me on Bitbucket', 'edubin'), 'class' => 'bitbucket'),
            array('url' => 'dribbble.com', 'icon' => 'flaticon-dribbble-logo', 'title' => esc_html__('Follow me on Dribbble', 'edubin'), 'class' => 'dribbble'),
            array('url' => 'facebook.com', 'icon' => 'flaticon-facebook-logo', 'title' => esc_html__('Like me on Facebook', 'edubin'), 'class' => 'facebook'),
            array('url' => 'flickr.com', 'icon' => 'flaticon-flickr-website-logo-silhouette', 'title' => esc_html__('Connect with me on Flickr', 'edubin'), 'class' => 'flickr'),
            array('url' => 'github.com', 'icon' => 'flaticon-github-character', 'title' => esc_html__('Fork me on GitHub', 'edubin'), 'class' => 'github'),
            array('url' => 'instagram.com', 'icon' => 'flaticon-instagram-logo', 'title' => esc_html__('Follow me on Instagram', 'edubin'), 'class' => 'instagram'),

            array('url' => 'linkedin.com', 'icon' => 'flaticon-linkedin-logo', 'title' => esc_html__('Connect with me on LinkedIn', 'edubin'), 'class' => 'linkedin'),
            array('url' => 'medium.com', 'icon' => 'flaticon-medium-size', 'title' => esc_html__('Folllow me on Medium', 'edubin'), 'class' => 'medium'),
            array('url' => 'pinterest.com', 'icon' => 'flaticon-pinterest', 'title' => esc_html__('Follow me on Pinterest', 'edubin'), 'class' => 'pinterest'),
            array('url' => 'plus.google.com', 'icon' => 'flaticon-logo', 'title' => esc_html__('Connect with me on Google+', 'edubin'), 'class' => 'googleplus'),
            array('url' => 'slack.com', 'icon' => 'fa-slack', 'title' => esc_html__('Join me on Slack', 'edubin'), 'class' => 'slack.'),
            array('url' => 'snapchat.com', 'icon' => 'flaticon-snapchat', 'title' => esc_html__('Add me on Snapchat', 'edubin'), 'class' => 'snapchat'),
            array('url' => 'soundcloud.com', 'icon' => 'flaticon-soundcloud', 'title' => esc_html__('Follow me on SoundCloud', 'edubin'), 'class' => 'soundcloud'),
            array('url' => 'stackoverflow.com', 'icon' => 'flaticon-overflowing-stacked-papers-tray', 'title' => esc_html__('Join me on Stack Overflow', 'edubin'), 'class' => 'stackoverflow'),
            array('url' => 'tumblr.com', 'icon' => 'flaticon-tumblr-logo', 'title' => esc_html__('Follow me on Tumblr', 'edubin'), 'class' => 'tumblr'),
            array('url' => 'twitter.com', 'icon' => 'flaticon-twitter-logo-silhouette', 'title' => esc_html__('Follow me on Twitter', 'edubin'), 'class' => 'twitter'),
            array('url' => 'vimeo.com', 'icon' => 'flaticon-vimeo-logo', 'title' => esc_html__('Follow me on Vimeo', 'edubin'), 'class' => 'vimeo'),
            array('url' => 'youtube.com', 'icon' => 'flaticon-youtube-logo', 'title' => esc_html__('Subscribe to me on YouTube', 'edubin'), 'class' => 'youtube'),
        );

        return apply_filters('edubin_social_icons', $social_icons);
    }
}

/**
 * Return an unordered list of linked social media icons, based on the urls provided in the Customizer Sortable Repeater
 * This is a sample function to display some social icons on your site.
 * This sample function is also used to show how you can call a PHP function to refresh the customizer preview.
 * Add the following to header.php if you want to see the sample social icons displayed in the customizer preview and your theme.
 * Before any social icons display, you'll also need to add the relevent URL's to the Header Navigation > Social Icons section in the Customizer.
 * <div class="social">
 *   <?php echo edubin_get_social_media(); ?>
 * </div>
 *
 * @return string Unordered list of linked social media icons
 */
if (!function_exists('edubin_get_social_media')) {
    function edubin_get_social_media()
    {
        $defaults         = edubin_generate_defaults();
        $output           = '';
        $social_icons     = edubin_generate_social_urls();
        $social_urls      = [];
        $social_newtab    = 0;
        $social_alignment = '';

        $social_urls      = explode(',', get_theme_mod('social_urls', $defaults['social_urls']));
        $social_newtab    = get_theme_mod('social_newtab', $defaults['social_newtab']);
        $social_alignment = get_theme_mod('social_alignment', $defaults['social_alignment']);

        foreach ($social_urls as $key => $value) {
            if (!empty($value)) {
                $domain = str_ireplace('www.', '', parse_url($value, PHP_URL_HOST));
                $index  = array_search($domain, array_column($social_icons, 'url'));
                if (false !== $index) {
                    $output .= sprintf('<li class="%1$s"><a href="%2$s" title="%3$s"%4$s><i class="glyph-icon %5$s"></i></a></li>',
                        $social_icons[$index]['class'],
                        esc_url($value),
                        $social_icons[$index]['title'],
                        (!$social_newtab ? '' : ' target="_blank"'),
                        $social_icons[$index]['icon']
                    );
                } else {
                    $output .= sprintf('<li class="nosocial"><a href="%2$s"%3$s><i class="glyph-icon %4$s"></i></a></li>',
                        $social_icons[$index]['class'],
                        esc_url($value),
                        (!$social_newtab ? '' : ' target="_blank"'),
                        'flaticon-world'
                    );
                }
            }
        }

        if (!empty($output)) {
            $output = '<ul class="social-icons ' . $social_alignment . '">' . $output . '</ul>';
        }

        return $output;
    }
}

/**
 * Append a search icon to the primary menu
 * This is a sample function to show how to append an icon to the menu based on the customizer search option
 * The search icon wont actually do anything
 */
if (!function_exists('edubin_add_search_menu_item')) {
    function edubin_add_search_menu_item($items, $args)
    {
        $defaults = edubin_generate_defaults();

        if (get_theme_mod('search_menu_icon', $defaults['search_menu_icon'])) {
            if ($args->theme_location == 'primary') {
                $items .= '<li class="menu-item menu-item-search"><a href="#" class="nav-search"><i class="glyph-icon flaticon-musica-searcher"></i></a></li>';
            }
        }
        return $items;
    }
}
add_filter('wp_nav_menu_items', 'edubin_add_search_menu_item', 10, 2);

/**
 * Set our Customizer default options
 */
if (!function_exists('edubin_generate_defaults')) {
    function edubin_generate_defaults()
    {
        $customizer_defaults = array(

            // Header
            'logo_size'               => '180',
            'sticky_logo'               => '',
            'preloader_show'            => 1,
            'breadcrumb_show'           => 1,
            'back_to_top_show'          => 1,

            'header_top_show'           => 0,
            'top_email'                 => '',
            'top_phone'                 => '',
            'top_massage'                 => '',
            'login_reg_show'            => 1,
            'custom_logout_link'         => '',
            'custom_login_link'         => '',
            'custom_register_link'      => '',
            'header_top_text_color'     => '',
            'header_top_link_color'     => '',
            'header_top_bg_color'       => '',

            'header_variations'         => 'header_v2',
            'sticky_header_enable'      => 1,
            'top_search_enable'      => 1,
            'top_cart_enable'      => 1,
            'home_menu_acive_color'      => 0,

            'social_newtab'             => 0,
            'social_urls'               => '',
            'social_alignment'          => 'alignright',
            'social_url_icons'          => '',
            'search_menu_icon'          => 0,

            // General
            'primary_color'             => '',
            'secondary_color'           => '',
            '3rd_color'                 => '',
            'link_color'                => '',
            'link_hover_color'          => '',
            'btn_color'                 => '',
            'btn_hover_color'           => '',
            'menu_text_color'           => '',
            'menu_hover_color'           => '',
            'sub_menu_text_color'           => '',
            'sub_menu_arrow_color'           => '',
            'sub_menu_border_color'           => '',
            'sub_menu_bg_color'           => '',
            'mobile_menu_icon_color'           => '',

            'body_fonts'                => '',
            'headings_fonts'            => '',
            'menu_font'                 => '',

            //Blog
            'blog_sidebar'              => 'alignright',
            'blog_author_show'          => 1,
            'blog_date_show'            => 1,
            'blog_category_show'        => 0,
            'blog_comment_show'         => 1,
            'blog_view_show'            => 0,

            'blog_single_sidebar'       => 'alignright',
            'blog_single_author_show'   => 1,
            'blog_single_date_show'     => 1,
            'blog_single_category_show' => 0,
            'blog_single_comment_show'  => 1,
            'blog_single_view_show'     => 0,

            // The Event Calendar 
            'tbe_price'         => 1,
            'tbe_archive_meta'         => 1,

            'edubin_tribe_events_layout'         => 'layout_1',
            'tbe_event_countdown'         => 1,
            'tbe_event_maps'         => 1,
            'tbe_start_time'         => 1,
            'tbe_end_time'         => 1,
            'tbe_website'         => 1,
            'tbe_phone'         => 1,
            'tbe_email'         => 1,
            'tbe_organizer_ids'         => 1,
            'tbe_location'         => 1,
            'tbe_content_before_massage'         => 1,
            'tbe_content_after_massage'         => 1,

            // WooCommerce
            'edubin_wc_sidebar'         => 'sidebarnone',

            // Event
            'event_list_style'          => 1,

            // 404
            'error_404_img'             => '',
            'error_404_heading'         => '404 ERROR!',
            'error_404_text'            => "Oops! The page you are looking for does not exist.",
            'error_404_link_text'       => "Go home",

            // Learnpress
            'lp_header_top'             => false,
            'lp_course_archive_style'   => '1',
            'lp_course_archive_clm'     => '4',
            'lp_price_show'             => true,
            'lp_review_on_off'          => true,
            'lp_instructor_img_on_off'  => true,
            'lp_instructor_name_on_off' => true,
            'lp_enroll_on_off'          => true,
            'lp_comment_show'           => false,

            'lp_instructor_single'      => true,
            'lp_cat_single'             => false,
            'lp_review_single'          => true,

            // learndash
            'ld_course_archive_style'   => '1',
            'ld_course_archive_clm'     => '4',
            'ld_price_show'             => true,
            'ld_views_show'             => true,
            'ld_comment_show'           => false,
            'see_more_btn'              => true,

            'ld_sidebar_single_show'    => true,
            'ld_related_course_show'    => true,
            'lp_course_feature_title'    => '',
            'lp_course_feature_quizzes_show'    => true,
            'lp_course_feature_duration_show'    => true,
            'lp_course_feature_max_students_show'    => true,
            'lp_course_feature_enroll_show'    => true,
            'lp_course_feature_retake_count_show'    => true,
            'lp_course_feature_skill_level_show'    => true,
            'lp_course_feature_language_show'    => true,
            'lp_course_feature_assessments_show'    => true,
            'lp_course_feature_quizzes'    => '',
            'lp_course_feature_duration'    => '',
            'lp_course_feature_max_tudents'    => '',
            'lp_course_feature_enroll'    => '',
            'lp_course_feature_retake_count'    => '',
            'lp_course_feature_skill_level'    => '',
            'lp_course_feature_language'    => '',
            'lp_course_feature_assessments'    => '',

            // Footer
            'footer_text_color'         => '',
            'footer_link_color'         => '',
            'footer_btn_submit_color'   => '',
            'footer_bg_color'           => '',
            'footer_widget_area_column' => '3_3_3_3',
            //Copyright
            'copyright_text'            => '&copy; 2019 <a href="' . esc_url('https://wpsprite.com') . '">Pixelcurve</a>. All rights reserved.',
            'copyright_text_color'      => '',
            'copyright_link_color'      => '',
            'copyright_bg_color'        => '',

        );

        return apply_filters('edubin_customizer_defaults', $customizer_defaults);
    }
}


/**
 * Adds the individual sections, settings, and controls to the theme customizer
 */
class edubin_initialise_customizer_settings
{
    // Get our default values
    private $defaults;

    public function __construct()
    {
        // Get our Customizer defaults
        $this->defaults = edubin_generate_defaults();

        // Register our Panels
        add_action('customize_register', array($this, 'edubin_add_customizer_panels'));

        // Register our sections
        add_action('customize_register', array($this, 'edubin_add_customizer_sections'));

    }
    //Customizer Panel
    public function edubin_add_customizer_panels($wp_customize)
    {
        $wp_customize->add_panel('general_panel',
            array(
                'title'    => esc_html__('General', 'edubin'),
                'priority' => 90,
            )
        );

        $wp_customize->add_panel('header_naviation_panel',
            array(
                'title'    => esc_html__('Header', 'edubin'),
                'priority' => 90,
            )
        );
        $wp_customize->add_panel('learnpress_panel',
            array(
                'title'    => esc_html__('LearnPress', 'edubin'),
                'priority' => 90,
            )
        );
        $wp_customize->add_panel('learndash_panel',
            array(
                'title'    => esc_html__('LearnDash', 'edubin'),
                'priority' => 90,
            )
        );        
        $wp_customize->add_panel('edubin_tribe_customizer_panel',
            array(
                'title'    => esc_html__('Events', 'edubin'),
                'priority' => 90,
            )
        );
        $wp_customize->add_panel('footer_panel',
            array(
                'title'    => esc_html__('Footer', 'edubin'),
                'priority' => 200,
            )
        );
    }

    //Customizer Settings
    public function edubin_add_customizer_sections($wp_customize)
    {

        // Move customizer default settings
        $wp_customize->add_section('title_tagline', array(
            'title'    => esc_html__('Logo', 'edubin'),
            'priority' => 20,
            'panel'    => 'general_panel',
        ));
        $wp_customize->add_section('colors', array(
            'title'    => esc_html__('Colors', 'edubin'),
            'priority' => 40,
            'panel'    => 'general_panel',
        ));
        $wp_customize->add_section('background_image', array(
            'title'          => esc_html__('Background Image', 'edubin'),
            'theme_supports' => 'custom-background',
            'priority'       => 80,
            'panel'          => 'general_panel',
        ));

        $wp_customize->add_section('error_404', array(
            'title'    => esc_html__('404 Page', 'edubin'),
            'priority' => 80,
            'panel'    => 'general_panel',
        ));

        $wp_customize->add_section('header_image', array(
            'title'          => esc_html__('Header Image', 'edubin'),
            'theme_supports' => 'custom-header',
            'priority'       => 200,
            'panel'          => 'header_naviation_panel',
        ));

        // Add light logo under Site Identity
        $wp_customize->add_setting('sticky_logo', array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'esc_url_raw',
            'transport'         => 'refresh',
            'default'           => $this->defaults['sticky_logo'],
        ));

        $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'sticky_logo', array(
            'label'       => esc_html__('Sticky Logo', 'edubin'),
            'description' => esc_html__('Sticky logo only for transparent header.', 'edubin'),
            'section'     => 'title_tagline',
            'settings'    => 'sticky_logo',
            'priority'    => 9,
        )));

        if (file_exists(get_template_directory() . '/admin/theme-panel/general.php')) {
            require_once get_template_directory() . '/admin/theme-panel/general.php';
        }
        if (file_exists(get_template_directory() . '/admin/theme-panel/header.php')) {
            require_once get_template_directory() . '/admin/theme-panel/header.php';
        }
        if (class_exists('LearnPress') && file_exists(get_template_directory() . '/admin/theme-panel/learnpress.php')) {
            require_once get_template_directory() . '/admin/theme-panel/learnpress.php';
        }
        if (class_exists('SFWD_LMS') && file_exists(get_template_directory() . '/admin/theme-panel/learndash.php')) {
            require_once get_template_directory() . '/admin/theme-panel/learndash.php';
        }
        if (class_exists('Tribe__Events__Main') && file_exists(get_template_directory() . '/admin/theme-panel/tribe_events.php')) {
            require_once get_template_directory() . '/admin/theme-panel/tribe_events.php';
        }        
        if (class_exists('WooCommerce') && file_exists(get_template_directory() . '/admin/theme-panel/woocommerce.php')) {
            require_once get_template_directory() . '/admin/theme-panel/woocommerce.php';
        }
        if (file_exists(get_template_directory() . '/admin/theme-panel/color.php')) {
            require_once get_template_directory() . '/admin/theme-panel/color.php';
        }
        if (file_exists(get_template_directory() . '/admin/theme-panel/typography.php')) {
            require_once get_template_directory() . '/admin/theme-panel/typography.php';
        }
        if (file_exists(get_template_directory() . '/admin/theme-panel/social.php')) {
            require_once get_template_directory() . '/admin/theme-panel/social.php';
        }
        if (file_exists(get_template_directory() . '/admin/theme-panel/footer.php')) {
            require_once get_template_directory() . '/admin/theme-panel/footer.php';
        }
        if (file_exists(get_template_directory() . '/admin/theme-panel/blog.php')) {
            require_once get_template_directory() . '/admin/theme-panel/blog.php';
        }
    }

}

/**
 * Load all our Customizer Custom Controls
 */
require_once trailingslashit(dirname(__FILE__)) . 'custom-controls.php';

/**
 * Initialise our Customizer settings
 */
$edubin_settings = new edubin_initialise_customizer_settings();
