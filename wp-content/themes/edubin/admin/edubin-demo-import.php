<?php
function edubin_import_files() {
    return array(
        array(
            'import_file_name'             => 'LearnPress',
            'categories'                   => array( 'LearnPress' ),
            'local_import_file'            => trailingslashit( get_template_directory() ) . 'admin/demo/learnpress/content.xml',
            'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'admin/demo/learnpress/widget_data.wie',
            'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'admin/demo/learnpress/customizer.dat',
            'import_preview_image_url'     => trailingslashit( get_template_directory_uri() ) . '/screenshot.png',
            'preview_url'                  => 'https://wpsprite.com/wordpress/edubin/',
            'import_notice'              =>    esc_html__( 'By importing it you will get LearnPress LMS demos. For import you need to wait few minutes.', 'edubin' ),
        ),
        array(
            'import_file_name'             => 'LearnDash',
            'categories'                   => array( 'LearnDash' ),
            'local_import_file'            => trailingslashit( get_template_directory() ) . 'admin/demo/learndash/content.xml',
            'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'admin/demo/learndash/widget_data.wie',
            'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'admin/demo/learndash/customizer.dat',
            'import_preview_image_url'     => trailingslashit( get_template_directory_uri() ) . '/screenshot.png',
            'preview_url'                  => 'https://wpsprite.com/wordpress/edubin/learndash',
            'import_notice'              =>    __( '<a target="_blank" class="button edubin-ld-video-link" href="https://youtu.be/_qg0igPtPF8">Watch LearnDash Setup Video</a> At first, deactivate all other LMS plugins, reset your site permalink and install the "LearnDash" plugin then import your LearnDash demo. ', 'edubin' ),
        ),

    );
}
add_filter( 'pt-ocdi/import_files', 'edubin_import_files' );


function edubin_dialog_options ( $options ) {
    return array_merge( $options, array(
        'width'       => 300,
        'dialogClass' => 'wp-dialog',
        'resizable'   => false,
        'height'      => 'auto',
        'modal'       => true,
    ) );
}
add_filter( 'pt-ocdi/confirmation_dialog_options', 'edubin_dialog_options', 10, 1 );
add_filter( 'pt-ocdi/disable_pt_branding', '__return_true' );

function edubin_after_import_setup() {
    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Primary', 'nav_menu' );
    $footer_menu = get_term_by( 'name', 'Footer Menu', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
            'footer_menu' => $footer_menu->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    $blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'edubin_after_import_setup' );

?>